'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('DOC_INV_FASE_Pies', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      PAQUETE_INVENTARIOS: {
        type: Sequelize.STRING
      },
      DOCUMENTO_INV: {
        type: Sequelize.STRING
      },
      LINEA_DOC_INV: {
        type: Sequelize.INTEGER
      },
      FASE: {
        type: Sequelize.STRING
      },
      ORDEN_CAMBIO: {
        type: Sequelize.INTEGER
      },
      RowPointer: {
        type: Sequelize.STRING
      },
      NoteExistsFlag: {
        type: Sequelize.INTEGER
      },
      RecordDate: {
        type: Sequelize.TIME
      },
      CreatedBy: {
        type: Sequelize.STRING
      },
      UpdatedBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('DOC_INV_FASE_Pies');
  }
};
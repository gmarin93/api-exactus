// Obtener fecha
const getFecha = () => {
    let day = new Date().getDate();
    if (day.toString().length === 1) {
      day = `0${day}`;
    }
    let month = new Date().getMonth() + 1;
    if (month.toString().length === 1) {
      month = `0${month}`;
    }
    let year = new Date().getFullYear();
    const fecha = `${year}-${month}-${day}`;
    return fecha;
  };
  const getHora = () => { 

    let hora = new Date().getHours();
    if (hora.toString().length === 1) {
      hora = `0${hora}`;
    }
    let minutos = new Date().getMinutes();
    if (minutos.toString().length === 1) {
      minutos = `0${minutos}`;
    }
    const concatenarHora = `${hora}:${minutos}:00`;  
  
  return concatenarHora;
  };

  module.exports = {
    getFecha,
    getHora
  };
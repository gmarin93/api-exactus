/**
 * Se aplica formato al ID obtenido
 * @param {EL ultimo ID del registro almacenado en Solicid-OC de Exactus} lastId
 */

const formatearConsecutivo = (lastId) => { 
    lastId = lastId.replace(/['"]+/g, ""); //Se quitan las dobles comillas
    let numId = parseInt(lastId.substr(2, lastId.length)); //Se obtiene el numero entero de la llave pk
    numId = numId + 1;
    numId = Array.from(String(numId)); //Se parsea a Arreglo
    lastId = lastId.substr(0, lastId.length - numId.length); //Se obtienen los ceros y letras del numero de documento para agregarle el numero entero sumado, asi creando un consecutivo por defecto.
    lastId = `${lastId}${numId.toString().replace(/['",]+/g, "")}`; //Se unen los datos para obtener el codigo nuevo
    return lastId;
  };

  module.exports = {
    formatearConsecutivo
  };
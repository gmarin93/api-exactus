var express = require("express");
var router = express.Router();
var models = require("../models");
const Sequelize = require("sequelize");

router.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  // cors();
  next();
});

// Obtiene todos los articulos de la bodega de agroquimicos Exactus
async function obtenerArticulos(req, res) {
    
  const articulos = await models.ARTICULO.articulosExactus();

  res.json(articulos);
}

// Obtiene todos los choferes
async function obtenerChoferes(req, res) {
    
  const choferes = await models.EMPLEADO.choferes();

  res.json(choferes);
}

router.get("/articulos", obtenerArticulos);

router.get("/choferes", obtenerChoferes);

module.exports = router;

var express = require('express');
var router = express.Router();
const maderas = require('../controllers/maderas')


router.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  // cors();
  next();
});



// Obtener los centro de costo de maderas
router.get('/centrocostos', maderas.obtenerCentroCosto);

// Obtener las cuentas contables de maderas
router.get('/cuentascontables', maderas.obtenerCuentaContables);

// Obtener las labores
router.get('/labores', maderas.obtenerLabores);

// Obtener empleados
router.get('/empleados', maderas.obtenerEmpleados);

// Obtener empleados por codigo
router.get('/empleados/:codigo', maderas.obtenerEmpleadosPorCodigo);

// Obtener insumos
router.get('/insumos', maderas.obtenerInsumos);

// Obtener insumos
router.get('/bodegas', maderas.obtenerBodegas);

// Obtener insumo por codigo
router.get('/insumos/:codigo', maderas.obtenerInsumoPorCodigo);

// Obtener proyeccion de insumos
router.post('/proyeccion', maderas.proyeccionInsumos);

// Crear requisicion
router.post('/', maderas.crearRequisicion);
  
router.get('/proyectos/fases', maderas.obtenerProyectosFases);

router.get('/fases/labores', maderas.obtenerFasesLabores);
  
  
module.exports = router;
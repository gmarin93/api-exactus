const express = require('express');
const router = express.Router();
const empleados = require('../controllers/empleados')

router.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  // cors();
  next();
});

// Obtener los empleados de maquinaria
router.get('/', empleados.obtenerEmpleadosMaquinaria); 

// Obtener todos los empleados de La lydia y Pitalia Activos
router.get('/activos', empleados.obtenerEmpleadosTodos); 

// Obtener todos los empleados de La lydia y Pitalia Activos
router.get('/inactivos', empleados.obtenerInactivos); 

// Obtener datos para creacion de planillas
router.get('/planillas', empleados.obtenerDatosParaPlanilla); 

// Obtener todos los empleados de La lydia y Pitalia 
router.get('/todos', empleados.obtenerEmpleadosTodosParaIncapacidades); 

// Obtener todos los empleados de La lydia 
router.get('/lydia', empleados.obtenerEmpleadosLaLydia); 


// Obtener empleado por codigo
router.get('/:codigo', empleados.obtenerEmpleadoPorCodigo); 

// Obtener los empleados de maquinaria
router.get('/departamentos/obtener', empleados.obtenerDepartamentos); 

// Creacion de incapacidad
router.post('/crear_incapacidad', empleados.crearIncapacidad); 
  
// Eliminar incapacidad
router.put('/eliminar_incapacidad', empleados.eliminarAccionPersonal); 
  
  
module.exports = router;
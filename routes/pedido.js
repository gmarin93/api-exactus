var express = require('express');
var router = express.Router();
const pedido = require('../controllers/pedido')


router.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  // cors();
  next();
});

//lista de precios paletas para empaque de paletas por semana
router.post('/lista_precios/:finicio/:ffin', pedido.listaPreciosArticulos);

//lista de precios paletas
router.post('/lista_precios', pedido.listaPreciosPedido);

// Creacion orden pedido
router.post('/crear_pedido/:cliente/:factura', pedido.crearOrdenPedido);

// Direcciones del cliente
router.get('/direcciones/:cliente', pedido.direccionesCliente);

// Obtener los clientes
router.get('/pedido_cliente/:pedido', pedido.getPedidoCliente);

// Obtener los clientes
router.get('/clientes', pedido.getClientes);

// Obtener las facturas
router.post('/facturas', pedido.getFacturas);
  
  
module.exports = router;
var express = require('express');
var router = express.Router();
const inventario = require('../controllers/inventario')


router.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  // cors();
  next();
});

// Creacion orden invetario
router.post('/', inventario.cargarInventario);

  
  
module.exports = router;
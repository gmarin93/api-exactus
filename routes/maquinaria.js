const express = require('express');
const router = express.Router();
const maquinaria = require('../controllers/maquinaria')


router.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  // cors();
  next();
});

// Obtener la maquinaria
router.get('/', maquinaria.obtenerMaquinarias); 
  
  
module.exports = router;
var express = require('express');
const { v4: uuidv4 } = require('uuid');
var router = express.Router();
var models = require('../models');
const Sequelize = require('sequelize');
// var cors = require('cors');


router.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  // cors();
  next();
});

// Se obtiene el último ID registrado en la tabla SolicitudOc
router.get('/ultimoidsolictudoc', function (req, res, next) {

    models.SOLICITUD_OC.ultimoIdSolicitudOc().then(id => {
      res.json({ id: id });
    });
  
  });
  // Se valida que el Id exista en la tabla SolicitudOccreate
  router.get('/validarsolicitudoc/:id', function (req, res, next) {
  
    const id =req.params.id;
  
    models.SOLICITUD_OC.validarId(id).then(id => {
      res.json({ id: id });
    });
  
  });

   // Solicitudes enviadas en un rango de fechas
   router.get('/solicitudes/:fechaInicioC/:fechaFinalC/:fechaA', function (req, res, next) {
  
    const {fechaInicioC, fechaFinalC, fechaA} = req.params;
    models.SOLICITUD_OC_LINEA.obtenerSolicitudes(fechaInicioC, fechaFinalC, fechaA).then(solicitudes => {
      res.json({ solicitudes });
    });
  
  });

  // Se registra en la base de datos la solicitud de la proyeccion
  router.post('/registrosolicitud', async function (req, res, next) {

    let dataGen = (req.body);
    // res.json(dataGen)
    let object = {
        RowPointer: uuidv4(),
        SOLICITUD_OC : dataGen.SOLICITUD_OC,
        DEPARTAMENTO :dataGen.DEPARTAMENTO,
        FECHA_SOLICITUD : dataGen.FECHA_SOLICITUD,
        FECHA_REQUERIDA : dataGen.FECHA_REQUERIDA,
        PRIORIDAD : dataGen.PRIORIDAD,
        ESTADO : dataGen.ESTADO,
        USUARIO : dataGen.USUARIO,
        LINEAS_NO_ASIG : dataGen.LINEAS_NO_ASIG,
        FECHA_HORA : dataGen.FECHA_HORA,
        COMENTARIO : dataGen.COMENTARIO
    };
      
      models.SOLICITUD_OC.create(object).then(a => {
        console.log(a);
        if (dataGen.datos) {
    
          dataGen.datos.map(async e => {
    
            e.SOLICITUD_OC = dataGen.SOLICITUD_OC;
    
          await models.SOLICITUD_OC_LINEA.create(e)
            .then(resp=>{
              // console.log(resp);
            })
            .catch(error=>{
              console.log(error)
              res.json(error);
            })
          })
        }
        res.json(a);
      })
      .catch(error=>{
        console.log(error)
        res.json(error);
      })

        // res.json(dataGen)
  });
  
  
  module.exports = router;
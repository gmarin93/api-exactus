var express = require('express');
var router = express.Router();
var models = require('../models');
const Sequelize = require('sequelize');
// var cors = require('cors');


router.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  // cors();
  next();
});

/*método para consultar el consumo general por fecha*/
router.get('/consumoGeneral/:fechaInicioC/:fechaFinalC', function (req, res, next) {
  var fechaInicioC = req.params.fechaInicioC;
  var fechaFinalC = req.params.fechaFinalC;
  models.bodegas.consumoGeneral(fechaInicioC, fechaFinalC).then(consumoGeneral => {
    res.json({ consumoGeneral: consumoGeneral });
  });
});

/*método para consultar la nomina y los materiales por rango de fechas y un lote en específico*/
router.get('/cargaDatos/:fechaInicio/:fechaFinal/:centroCosto', function (req, res, next) {
  var fechaInicio = req.params.fechaInicio;
  var fechaFinal = req.params.fechaFinal;
  var centroCosto = req.params.centroCosto;

  var nomina = models.bodegas.generalNominaLote(fechaInicio, fechaFinal, centroCosto);
  var materiales = models.bodegas.materialesLote(fechaInicio, fechaFinal, centroCosto);
  res.format({
    json: function () {
      Promise.all([nomina, materiales]).then(values => {
        nomina.then(nomina => {
          res.json({ nomina: values[0], materiales: values[1] });
        });
      });
    }
  });
});


router.get('/cuentas', function (req, res, next) {

  models.bodegas.cuentaCombustible().then(cuentas => {
    res.json({ cuentas: cuentas });
  });
});

// Se obtiene el último ID registrado en la tabla
router.get('/ultimoidreg', function (req, res, next) {

  models.DOCUMENTO_INV.últimoID().then(id => {
    res.json({ id: id });
  });

});


// Se obtiene el último ID registrado en la tabla
router.get('/validaridreg/:doc', function (req, res, next) {

  var doc=req.params.doc;

  models.DOCUMENTO_INV.validarID(doc).then(id => {
    res.json({ id: id });
  });

});

// Ruta para verificar si existe el centro de cost
router.get('/validar_centrocosto/:centro_costo/:cuenta_contable', async function (req, res, next) {

  const {centro_costo,cuenta_contable} = req.params; 
  try {    
    const costo = await models.DOCUMENTO_INV.validarCentroCosto(centro_costo);
    const cuenta = await models.DOCUMENTO_INV.validarCuentaContable(cuenta_contable);
  
    if(costo.length > 0 && cuenta.length > 0){
      res.json({"valido":true})
    }else{
      res.json({"valido":false})
    }

  } catch (error) {
    res.status(500).json(error)
  }


});





// Se crea la requisicion de exactus Document Inv
router.post('/registrodocumento', async function (req, res, next) {

  let dataGen = (req.body);
  let response='';
  // res.json(dataGen)
  let object = {
    REFERENCIA: dataGen.REFERENCIA,
    SELECCIONADO: dataGen.SELECCIONADO,
    USUARIO: dataGen.USUARIO,
    MENSAJE_SISTEMA: dataGen.MENSAJE_SISTEMA,
    CONSECUTIVO: dataGen.CONSECUTIVO,
    CREATEDBY: dataGen.CREATEDBY,
    PAQUETE_INVENTARIO: dataGen.PAQUETE_INVENTARIO,
    CUENTA_CONTABLE: dataGen.CUENTA_CONTABLE,
    DOCUMENTO_INV: dataGen.DOCUMENTO_INV,
    FECHA_HOR_CREACION: dataGen.FECHA_HOR_CREACION,
    FECHA_DOCUMENTO: dataGen.FECHA_DOCUMENTO,
    FECHA_HORA_APROB: dataGen.FECHA_HORA_APROB
  };

  let crear = false;

    dataGen.datos.map(m => {
      if(!m.estado_aprobar){
        crear = true;
        return;
      }
    })
    
    if(crear){
      models.DOCUMENTO_INV.create(object).then(async(a) => {

        if (dataGen.datos) {
          for (const e of dataGen.datos) {
            e.DOCUMENTO_INV = dataGen.DOCUMENTO_INV;
            e.ORDEN_CAMBIO = 0;
            if(!e.estado_aprobar){
              await models.LINEA_DOC_INV.create(e)
              // Crear DOC_INV_FASE_PY
              await models.DOC_INV_FASE_PY.create(e)
            } 
          }

        // Actualiza el consecutivo del PEDIDO
        await  models.CONSECUTIVO_FA.updateConsumo(dataGen.DOCUMENTO_INV);
    
        }
        res.json(a);
      });
    }else{
      res.json({RowPointer:'12345'});
    }
   
});



async function ObtenerArticulos (req,res,next){

  const Ids =(req.params.array);
  const fechaI = req.params.fechaI;
  const fechaF = req.params.fechaF;

  const resultadoArticulos = await models.ARTICULO.obtenerArticulos(Ids);
  const resultadoOrdenes = await models.ARTICULO.obtenerOrdenesCompra(fechaI,fechaF);

  Promise.all([resultadoArticulos,resultadoOrdenes]).then((values) => {

    res.json({
        articulos:values[0],
        ordenes: values[1]
    });
});
}

async function ExistenciaArticulos (req,res,next){

  const Ids =(req.params.array);

  const articulos = await models.ARTICULO.obtenerArticulos(Ids);  

    res.json({ articulos});
}


async function fasesExactus (req,res,next){

  const articulos = await models.ARTICULO.obtenerFaseseAgro();  

    res.json( articulos);
}




/*método para consultar el consumo por lote*/
router.get('/consumoLote/:fecha/:fechaFinal/:centroCosto', function (req, res, next) {

  var centroCosto = req.params.centroCosto;
  var fecha = req.params.fecha;
  var fechaFinal = req.params.fechaFinal;

  models.bodegas.consumoLote(fecha, fechaFinal, centroCosto).then(consumoLote => {
    res.json({ consumoLote: consumoLote});
  });

});

// /**
//  * Obtiene las ordenes de compra que obtienen los datos de insumos en transito
//  */
// async function ObtenerOrdenesCompra (req,res,next){

//   const fechaI = req.params.fechaI;
//   const fechaF = req.params.fechaF;

//     const resultado = await models.ARTICULO.obtenerOrdenesCompra(fechaI,fechaF);

//     res.json(resultado);

// }


//Obtiene los insumos proyectados de cada opcion de aplicacion
// router.get('/obtenerArticulos/:array',ObtenerArticulos);
router.get('/obtenerArticulos/:array/:fechaI/:fechaF',ObtenerArticulos);

router.get('/existenciaArticulos/:array',ExistenciaArticulos);

router.get('/fases',fasesExactus);

// router.get('/obtenerOrdenesCompra/exactus/:fechaI/:fechaF',ObtenerOrdenesCompra);


module.exports = router;

var express = require('express');
var router = express.Router();
const ganadero = require('../controllers/ganadero')


router.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  // cors();
  next();
});

// Obtener insumos
router.get('/insumos', ganadero.obtenerInsumos);

// Obtener insumo por codigo
router.get('/insumos/:codigo', ganadero.obtenerInsumoPorCodigo);

// Obtener insumo por codigo
router.get('/bodegas_insumos/:codigo', ganadero.obtenerBodegasInsumos);

// Obtener centro costo
router.get('/centro_costo', ganadero.obtenerCentrosCostoGanadero);

// Obtener centro costo
router.get('/cuenta_contables', ganadero.obtenerCuentaContableGanadero);

// Crear requisicion
router.post('/', ganadero.crearRequisicion);
  
module.exports = router;
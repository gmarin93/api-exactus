var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');

var datos = require('./routes/datos');

const proyeccion = require('./routes/proyeccion');
// Rutas para sicronizar tablas
const sincronizacion = require('./routes/sincronizacionTablas');
// Rutas para pedido prefacturacion eAgro
const pedido = require('./routes/pedido');
// Rutas para cargar inventario eAgro
const inventario = require('./routes/inventario');
// Rutas para maderas Software Maderas
const maderas = require('./routes/maderas');
// Rutas para maquinaria
const maquinaria = require('./routes/maquinaria');
// Rutas para ganaderia
const ganadero = require('./routes/ganadero');
// Rutas para empleados Maquinaria
const empleados = require('./routes/empleados');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/api/v1/datos',datos)

// ruta de sincronizacion de tablas exactus sincronizacion
app.use('/api/v1/datos',datos)

// ruta de proyeccion insumos
app.use('/api/v1/proyeccion',proyeccion)

//rutas de sincronizacion de insumos
app.use('/api/v1/sincronizacion',sincronizacion);

//rutas de pedido prefacturacion
app.use('/api/v1/pedido',pedido);

//rutas de inventario cargar eAgro
app.use('/api/v1/inventario',inventario);

//rutas de maderas Software Maderas La Lydia
app.use('/api/v1/maderas',maderas);

//rutas de maquinaria La Lydia
app.use('/api/v1/maquinaria',maquinaria);

//rutas de ganadero La Lydia
app.use('/api/v1/ganadero',ganadero);

//rutas de empleados Maquinaria La Lydia
app.use('/api/v1/empleados',empleados);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class PROYECCION extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }

  /**
   * Obtiene las ordenes de compra de una lista de insumos
   * @param {* Insumos } insumos 
   * @returns 
   */
  PROYECCION.obtenerOrdenCompraInsumos = (fechaI,fechaF,insumos) => {
    return sequelize.query(
     `select ocl.ARTICULO as codigo, sum(ocl.CANTIDAD_ORDENADA) as ordenado from lalydia.ORDEN_COMPRA oc       
      inner join lalydia.ORDEN_COMPRA_LINEA ocl on oc.ORDEN_COMPRA  = ocl.ORDEN_COMPRA
      where oc.FECHA_HORA_CONFIR  between '${fechaI}' and '${fechaF}' and oc.BODEGA LIKE '08'
      and ocl.ARTICULO in (${insumos})
      group by ocl.ARTICULO `,
      { type: sequelize.QueryTypes.SELECT }
    );
  };

  /**
   * Obtiene la existencia de una lista de insumos
   * @param {* Insumos para saber cantidad orden de compra} insumos 
   * @returns 
   */
   PROYECCION.existenciaInsumos = (insumos) => {
    return sequelize.query(
      `select a.ARTICULO as codigo,a.DESCRIPCION,eb.CANT_DISPONIBLE as existencia from lalydia.ARTICULO a 
      inner join lalydia.EXISTENCIA_BODEGA eb on eb.ARTICULO = a.ARTICULO 
      where a.ARTICULO in (${insumos}) and eb.BODEGA LIKE '08'`,
      { type: sequelize.QueryTypes.SELECT }
    );
  };

  return PROYECCION;
};

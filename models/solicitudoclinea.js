'use strict';
module.exports = (sequelize, DataTypes) => {
var SOLICITUD_OC_LINEA = sequelize.define('SOLICITUD_OC_LINEA', {
  RowPointer: { type: DataTypes.STRING, autoIncrement: true, primaryKey: true },
  SOLICITUD_OC: DataTypes.STRING,
  SOLICITUD_OC_LINEA: DataTypes.INTEGER,
  USUARIO_CANCELA: DataTypes.STRING,
  ARTICULO: DataTypes.STRING,
  DESCRIPCION: DataTypes.STRING,
  CANTIDAD: DataTypes.DECIMAL,
  SALDO: DataTypes.DECIMAL,
  ESTADO: DataTypes.STRING,
  COMENTARIO: DataTypes.STRING,
  FECHA_REQUERIDA: DataTypes.TIME,
  UNIDAD_DISTRIBUCIO: DataTypes.STRING,
  FECHA_HORA_CANCELA: DataTypes.TIME,
  CENTRO_COSTO: DataTypes.STRING,
  CUENTA_CONTABLE: DataTypes.STRING,
  FASE: DataTypes.STRING,
  PROYECTO: DataTypes.STRING,
  ORDEN_CAMBIO: DataTypes.INTEGER,
  CreatedBy:DataTypes.STRING,  
  UpdatedBy: DataTypes.STRING,
  CreateDate: DataTypes.TIME
}, {
 timestamps: false,
 hasTrigger: true,
 freezeTableName: true,
 tableName: 'SOLICITUD_OC_LINEA'
});

SOLICITUD_OC_LINEA.obtenerSolicitudes = function (fechaI,fechaF,fechaA){
  return sequelize.query(
    `select articulo from lalydia.SOLICITUD_OC_LINEA   where FECHA_REQUERIDA  between '${fechaI}' and '${fechaF}' or FECHA_REQUERIDA = '${fechaA}'`,
     { type: sequelize.QueryTypes.SELECT }
  )

}


return SOLICITUD_OC_LINEA;
};

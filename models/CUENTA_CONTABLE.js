'use strict';
module.exports = (sequelize, DataTypes) => {
var CUENTA_CONTABLE = sequelize.define('CUENTA_CONTABLE', {
  // RowPointer: { type: DataTypes.STRING, primaryKey: true }, 
  // CUENTA_CONTABLE	: DataTypes.STRING,
  // UBICACION	: DataTypes.STRING,
  // TIPO_ACTIVO	: DataTypes.STRING,
  // DESCRIPCION	: DataTypes.STRING,
  // FECHA_ULT_MANT	: DataTypes.TIME,
  // FECHA_PROX_MANT	: DataTypes.TIME,
  // ESTADO_ACTIVO	: DataTypes.STRING,
  // TASA_DEPRECIACION_F	: DataTypes.DECIMAL,
  // TASA_DEPRECIACION_C	: DataTypes.DECIMAL,
  // CLASIFICACION	: DataTypes.STRING,
  // USA_COMPONENTE	: DataTypes.STRING,
  // NoteExistsFlag	: DataTypes.INTEGER,
  // RecordDate	: DataTypes.TIME,
  // CreatedBy	: DataTypes.STRING,
  // UpdatedBy	: DataTypes.STRING,
  // CreateDate	: DataTypes.TIME,
  
}, {
 timestamps: false,
 hasTrigger: true,
 freezeTableName: true,
 tableName: 'CUENTA_CONTABLE'
});


CUENTA_CONTABLE.obtenerCuentaContableGanadero = () => {
  return sequelize.query(
    `select CUENTA_CONTABLE as value, concat(CUENTA_CONTABLE ,' -> ',DESCRIPCION) as label 
    from lalydia.CUENTA_CONTABLE cc  where CUENTA_CONTABLE in ('5-03-01-03-004','5-01-02-01-001','5-01-02-01-002','5-01-02-01-004','6-01-02-01-001','6-01-02-01-006')
    order by DESCRIPCION`,
    { type: sequelize.QueryTypes.SELECT }
  );
};

CUENTA_CONTABLE.obtenerCuentaContableMaderas = () => {
  return sequelize.query(
    `select CUENTA_CONTABLE as value, concat(CUENTA_CONTABLE, ' -> ',DESCRIPCION) as label 
    from lalydia.CUENTA_CONTABLE cc where ACEPTA_DATOS like 'S'
    order by CUENTA_CONTABLE`,
    { type: sequelize.QueryTypes.SELECT }
  );
};

return CUENTA_CONTABLE;
};

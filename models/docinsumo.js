'use strict';
module.exports = (sequelize, DataTypes) => {
var DOCUMENTO_INV = sequelize.define('DOCUMENTO_INV', {
  RowPointer: { type: DataTypes.STRING, autoIncrement: true, primaryKey: true },
  PAQUETE_INVENTARIO: DataTypes.STRING,
  DOCUMENTO_INV: DataTypes.STRING,
  REFERENCIA: DataTypes.STRING,
  FECHA_HOR_CREACION: DataTypes.TIME,
  FECHA_DOCUMENTO: DataTypes.TIME,
  SELECCIONADO: DataTypes.STRING,
  USUARIO:DataTypes.STRING,
  MENSAJE_SISTEMA:DataTypes.STRING,
  CONSECUTIVO:DataTypes.STRING,
  USUARIO_APRO:DataTypes.STRING,
  FECHA_HORA_APROB:DataTypes.TIME,
  APROBADO:DataTypes.STRING,
  externoid:DataTypes.STRING,
  NoteExistsFlag:DataTypes.STRING,
  RecordDate:DataTypes.TIME,
  CreatedBy: DataTypes.STRING,
  UpdatedBy: DataTypes.STRING,
  CreateDate: DataTypes.TIME
}, {
 timestamps: false,
 hasTrigger: true,
 freezeTableName: true,
 tableName: 'DOCUMENTO_INV'
});


   
DOCUMENTO_INV.últimoID= function(){
    
  return sequelize.query(
   `select SIGUIENTE_CONSEC as DOCUMENTO_INV from lalydia.CONSECUTIVO_CI cc where DESCRIPCION like 'CONSUMO'`,
   { type: sequelize.QueryTypes.SELECT}
  );
};

DOCUMENTO_INV.validarID= function(doc){
    
  return sequelize.query(
   `Select * from lalydia.DOCUMENTO_INV where DOCUMENTO_INV  like '${doc}'`,
   { type: sequelize.QueryTypes.SELECT}
  );
};

// Valida si existe el centro de costo
DOCUMENTO_INV.validarCentroCosto = function(centro_costo){    
  return sequelize.query(
   `SELECT CENTRO_COSTO FROM lalydia.CENTRO_COSTO where CENTRO_COSTO like '${centro_costo}' and ACEPTA_DATOS like 'S'`,
   { type: sequelize.QueryTypes.SELECT}
  );
};

// Valida si existe una cuenta contable
DOCUMENTO_INV.validarCuentaContable = function(cuenta_contable){    
  return sequelize.query(
   `SELECT CUENTA_CONTABLE FROM lalydia.CUENTA_CONTABLE where CUENTA_CONTABLE like '${cuenta_contable}' and ACEPTA_DATOS like 'S'`,
   { type: sequelize.QueryTypes.SELECT}
  );
};


DOCUMENTO_INV.associate = function(models) {
    
  DOCUMENTO_INV.hasMany(models.LINEA_DOC_INV, {   
    foreignKey:'DOCUMENTO_INV'
   });

};


return DOCUMENTO_INV;
};

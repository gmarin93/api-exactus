'use strict';
module.exports = (sequelize, DataTypes) => {
var DOC_INV_FASE_PY = sequelize.define('DOC_INV_FASE_PY', {
  RowPointer: { type: DataTypes.STRING, autoIncrement: true, primaryKey: true },
  PAQUETE_INVENTARIO: DataTypes.STRING,
  DOCUMENTO_INV: DataTypes.STRING,
  LINEA_DOC_INV: DataTypes.INTEGER,
  FASE: DataTypes.STRING,
  ORDEN_CAMBIO: DataTypes.INTEGER,
  NoteExistsFlag: DataTypes.INTEGER,
  RecordDate: DataTypes.TIME,
  CreatedBy: DataTypes.STRING,
  UpdatedBy: DataTypes.STRING,
  CreateDate: DataTypes.TIME
}, {
 timestamps: false,
 hasTrigger: true,
 freezeTableName: true,
 tableName: 'DOC_INV_FASE_PY'
});

return DOC_INV_FASE_PY;
};

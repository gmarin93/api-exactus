"use strict";
module.exports = (sequelize, DataTypes) => {
  var EMPLEADO_ACC_PER = sequelize.define(
    "EMPLEADO_ACC_PER",
    {
      RowPointer: { type: DataTypes.STRING, primaryKey: true },
      NUMERO_ACCION	: DataTypes.INTEGER,
      TIPO_ACCION	: DataTypes.STRING,
      FECHA	: DataTypes.TIME,
      EMPLEADO	: DataTypes.STRING,
      TIPO_AUSENCIA	: DataTypes.STRING,
      FECHA_RIGE	: DataTypes.TIME,
      FECHA_VENCE	: DataTypes.TIME,
      USUARIO	: DataTypes.STRING,
      FECHA_HORA	: DataTypes.TIME,
      ESTADO_ACCION	: DataTypes.STRING,
      DIAS_ACCION	: DataTypes.DECIMAL,
      SALDO	: DataTypes.DECIMAL,
      BONO_DECRETO	: DataTypes.DECIMAL,
      RecordDate	: DataTypes.TIME,
      CreatedBy	: DataTypes.STRING,
      UpdatedBy	: DataTypes.STRING,
      CreateDate	: DataTypes.TIME,
      RUBRO3:  DataTypes.STRING,
    },
    {
      timestamps: false,
      hasTrigger: true,
      freezeTableName: true,
      tableName: "EMPLEADO_ACC_PER",
    }
  );

  EMPLEADO_ACC_PER.obtenerUltimaAccion = (database) => {
    return sequelize.query(
      `select EMPLEADO,DEPARTAMENTO,NOMINA,PUESTO,PLAZA,CENTRO_COSTO,ESTADO_EMPLEADO,SALARIO_REFERENCIA from ${database} where EMPLEADO like '${empleado}'`,
       { type: sequelize.QueryTypes.SELECT }
    )
  }

  EMPLEADO_ACC_PER.obtenerEmpleado = (database,empleado) => {
    return sequelize.query(
      `select EMPLEADO,DEPARTAMENTO,NOMINA,PUESTO,PLAZA,CENTRO_COSTO,ESTADO_EMPLEADO,SALARIO_REFERENCIA from ${database} where EMPLEADO like '${empleado}'`,
       { type: sequelize.QueryTypes.SELECT }
    )
  }

  EMPLEADO_ACC_PER.crearIncapacidad = (database,NUMERO_ACCION,TIPO_ACCION,FECHA,EMPLEADO,TIPO_AUSENCIA,FECHA_RIGE,FECHA_VENCE,NOTAS,USUARIO,FECHA_HORA,ESTADO_ACCION,DIAS_ACCION,SALDO,RowPointer,BONO_DECRETO,RecordDate,CreatedBy,UpdatedBy,CreateDate,RUBRO3) => {
    return sequelize.query(
      ` INSERT INTO ${database} (NUMERO_ACCION,TIPO_ACCION,FECHA,EMPLEADO,TIPO_AUSENCIA,FECHA_RIGE,FECHA_VENCE,NOTAS,USUARIO,FECHA_HORA,ESTADO_ACCION,DIAS_ACCION,SALDO,RowPointer,BONO_DECRETO,RecordDate,CreatedBy,UpdatedBy,CreateDate,RUBRO3)
       VALUES(${NUMERO_ACCION},'${TIPO_ACCION}','${FECHA}','${EMPLEADO}','${TIPO_AUSENCIA}','${FECHA_RIGE}','${FECHA_VENCE}','${NOTAS}','${USUARIO}','${FECHA_HORA}','${ESTADO_ACCION}','${DIAS_ACCION}'
       ,${SALDO},'${RowPointer}',${BONO_DECRETO},'${RecordDate}','${CreatedBy}','${UpdatedBy}','${CreateDate}','${RUBRO3}');`,
      { type: sequelize.QueryTypes.SELECT }
    );
  };

  EMPLEADO_ACC_PER.obtenerNumeroAccion = (database) => {
    return sequelize.query(
      ` select ULTIMA_ACCION from ${database} `,
       { type: sequelize.QueryTypes.SELECT }
    )
  }

  EMPLEADO_ACC_PER.crearAccionPersonal = (database,numero_accion,empleado,departamento,puesto,plaza,centro_costo,nomina,estado_empleado,salario_promedio) => {
    return sequelize.query(
      `  INSERT INTO ${database} (numero_accion,empleado,departamento,puesto,plaza,centro_costo,nomina,regimen_vacacional,estado_empleado,salario_promedio)
      VALUES(${numero_accion},'${empleado}','${departamento}','${puesto}','${plaza}','${centro_costo}','${nomina}','NORMAL','${estado_empleado}',${salario_promedio});`,
       { type: sequelize.QueryTypes.SELECT }
    )
  }

  EMPLEADO_ACC_PER.actualizarGlobalGNPitalia = (database,numero_accion,fecha) => {
    return sequelize.query(
      `UPDATE  ${database} SET ULTIMA_ACCION = '${numero_accion}',RecordDate = '${fecha}'`,
       { type: sequelize.QueryTypes.SELECT }
    )
  }

  EMPLEADO_ACC_PER.actualizarGlobalGNLaLydia = (database,numero_accion,fecha) => {
    return sequelize.query(
      `UPDATE  ${database} SET ULTIMA_ACCION = '${numero_accion}'`,
       { type: sequelize.QueryTypes.SELECT }
    )
  }

  EMPLEADO_ACC_PER.validarAprobacionAccionPersonal = (database,numero_accion) => {
    return sequelize.query(
      `select NUMERO_ACCION from  ${database} where NUMERO_ACCION = '${numero_accion}' and ESTADO_ACCION like 'N'`,
       { type: sequelize.QueryTypes.SELECT }
    )
  }

  EMPLEADO_ACC_PER.eliminarAccionPersonal = (database,numero_accion) => {
    return sequelize.query(
      `delete from ${database} where NUMERO_ACCION = '${numero_accion}'`,
       { type: sequelize.QueryTypes.SELECT }
    )
  }







  return EMPLEADO_ACC_PER;
};

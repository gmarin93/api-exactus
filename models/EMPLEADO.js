"use strict";
module.exports = (sequelize, DataTypes) => {
  var EMPLEADO = sequelize.define(
    'EMPLEADO',
    {
      RowPointer: {
        type: DataTypes.STRING,
        autoIncrement: true,
        primaryKey: true,
      },
      EMPLEADO: DataTypes.INTEGER,
      NOMBRE: DataTypes.STRING,
      PUESTO: DataTypes.STRING,
      
    },
    {
      timestamps: false,
      hasTrigger: true,
      freezeTableName: true,
      tableName: "EMPLEADO",
    }
  );



  EMPLEADO.choferes = function () {
    return sequelize.query(
      `select * from lalydia.EMPLEADO where PUESTO like  'OPERBOOM'`,
       { type: sequelize.QueryTypes.SELECT }
    )
  }

  /*  Obtiene los empleado el en departamente de OM operadores **/
  EMPLEADO.obtenerEmpleadosMaquinaria = () => {
    return sequelize.query(
      `select EMPLEADO as value, CONCAT( NOMBRE,' -> (',EMPLEADO,')') as label  
      from lalydia.EMPLEADO e   
      where DEPARTAMENTO like 'OM' and ACTIVO LIKE 'S'
      order by NOMBRE `,
      { type: sequelize.QueryTypes.SELECT }
    );
  };

    /**
     * Obtiene la lista de empleados activos
     * @returns 
     */
    EMPLEADO.obtenerEmpleadosActivos = () => {
      return sequelize.query(
        `select EMPLEADO AS id,  concat(NOMBRE ,' (',EMPLEADO, ')') as name, IDENTIFICACION as identificacion ,NOMBRE as nombre  
        from lalydia.EMPLEADO e where ACTIVO like 's'
        order by NOMBRE`,
        { type: sequelize.QueryTypes.SELECT }
      );
    };

     /**
     * Obtiene la lista de empleados Inactivos La Lydia
     * @returns 
     */
     EMPLEADO.obtenerEmpleadosInactivosLaLydia = () => {
      return sequelize.query(
        `select EMPLEADO AS codigo, NOMBRE as nombre, ACTIVO 
        from lalydia.EMPLEADO e where ACTIVO like 'N'
        order by NOMBRE`,
        { type: sequelize.QueryTypes.SELECT }
      );
    };

     /**
     * Obtiene todos los empleados de La Lydia
     * @returns 
     */
     EMPLEADO.obtenerEmpleadosLaLydiaTodos = () => {
      return sequelize.query(
        `select EMPLEADO AS value,EMPLEADO AS id,  concat(NOMBRE ,' (',EMPLEADO, ')') as label, NOMBRE as name, NOMBRE as nombre,e.ACTIVO as activo,'La Lydia' as empresa,
        d.DESCRIPCION as departamento, d.JEFE AS encargado, p.DESCRIPCION as puesto, e.SEXO as sexo, e.PAIS as nacionalidad, 'false' as contratista
        from lalydia.EMPLEADO e 
        inner join lalydia.DEPARTAMENTO d on d.DEPARTAMENTO = e.DEPARTAMENTO 
        inner join lalydia.PUESTO p on p.PUESTO = e.PUESTO 
        order by NOMBRE`,
        { type: sequelize.QueryTypes.SELECT }
      );
    };

    /**
     * Obtiene la lista de empleados activos de PITALIA
     * @returns 
     */
    EMPLEADO.obtenerEmpleadosActivosPitalia = () => {
      return sequelize.query(
        `select EMPLEADO AS id,  concat(NOMBRE ,' (',EMPLEADO, ')') as name, IDENTIFICACION as identificacion ,NOMBRE as nombre  
        from PITALIA.EMPLEADO e where ACTIVO like 's'
        order by NOMBRE`,
        { type: sequelize.QueryTypes.SELECT }
      );
    };

     /**
     * Obtiene la lista de empleados Inactivos de PITALIA
     * @returns 
     */
     EMPLEADO.obtenerEmpleadosInactivosPitalia = () => {
      return sequelize.query(
        `select EMPLEADO AS codigo, NOMBRE as nombre, ACTIVO 
        from PITALIA.EMPLEADO e where ACTIVO like 'N'
        order by NOMBRE`,
        { type: sequelize.QueryTypes.SELECT }
      );
    };

    /**
     * Obtiene todos los empleados de Pitalia
     * @returns 
     */
    EMPLEADO.obtenerEmpleadosPitaliaTodos = () => {
      return sequelize.query(
        `select EMPLEADO AS value,  concat(NOMBRE ,' (',EMPLEADO, ')') as label, NOMBRE as nombre,e.ACTIVO as activo,'Pitalia' as empresa,
        d.DESCRIPCION as departamento, d.JEFE AS encargado, p.DESCRIPCION as puesto, e.SEXO as sexo, e.PAIS as nacionalidad
        from PITALIA.EMPLEADO e 
        inner join PITALIA.DEPARTAMENTO d on d.DEPARTAMENTO = e.DEPARTAMENTO 
        inner join PITALIA.PUESTO p on p.PUESTO = e.PUESTO 
        order by NOMBRE`,
        { type: sequelize.QueryTypes.SELECT }
      );
    };

    /**
     * Obtiene la lista de depatamentos
     * @returns 
     */
     EMPLEADO.obtenerDepartamentos = () => {
      return sequelize.query(
        `select DESCRIPCION as value, DESCRIPCION  as label from lalydia.DEPARTAMENTO d order by DESCRIPCION `,
        { type: sequelize.QueryTypes.SELECT }
      );
    };

       /**
     * Obtiene la lista de depatamentos de PITALIA
     * @returns 
     */
       EMPLEADO.obtenerDepartamentosPitalia = () => {
        return sequelize.query(
          `select CONCAT(DESCRIPCION, '-', 'PITALIA')  as value, CONCAT(DESCRIPCION, '-', 'PITALIA')  as label from PITALIA.DEPARTAMENTO d order by DESCRIPCION`,
          { type: sequelize.QueryTypes.SELECT }
        );
      };

    /**
     * Obtiene la informacion del empleado de Pitalia
     * @param {*} codigo 
     * @returns 
     */
        EMPLEADO.infoEmpleado = (codigo) => {
          return sequelize.query(
            `select PRIMER_APELLIDO ,SEGUNDO_APELLIDO ,NOMBRE_PILA,EMPLEADO,NOMBRE,ACTIVO,IDENTIFICACION,ASEGURADO,FECHA_INGRESO,SALARIO_REFERENCIA,SEXO
            from PITALIA.EMPLEADO e where EMPLEADO like '${codigo}'`,
            { type: sequelize.QueryTypes.SELECT }
          );
        };
  


  return EMPLEADO;
};

'use strict';
module.exports = (sequelize, DataTypes) => {
var ACTIVO_FIJO = sequelize.define('ACTIVO_FIJO', {
  RowPointer: { type: DataTypes.STRING, primaryKey: true }, 
  ACTIVO_FIJO	: DataTypes.STRING,
  UBICACION	: DataTypes.STRING,
  TIPO_ACTIVO	: DataTypes.STRING,
  DESCRIPCION	: DataTypes.STRING,
  FECHA_ULT_MANT	: DataTypes.TIME,
  FECHA_PROX_MANT	: DataTypes.TIME,
  ESTADO_ACTIVO	: DataTypes.STRING,
  TASA_DEPRECIACION_F	: DataTypes.DECIMAL,
  TASA_DEPRECIACION_C	: DataTypes.DECIMAL,
  CLASIFICACION	: DataTypes.STRING,
  USA_COMPONENTE	: DataTypes.STRING,
  NoteExistsFlag	: DataTypes.INTEGER,
  RecordDate	: DataTypes.TIME,
  CreatedBy	: DataTypes.STRING,
  UpdatedBy	: DataTypes.STRING,
  CreateDate	: DataTypes.TIME,
  
}, {
 timestamps: false,
 hasTrigger: true,
 freezeTableName: true,
 tableName: 'ACTIVO_FIJO'
});


ACTIVO_FIJO.obtenerMaquinaria = () => {
  return sequelize.query(
    `select  af.ACTIVO_FIJO as id,CONCAT(af.ACTIVO_FIJO,' -> ', mj.DESCRIPCION ) as name,  mj.DESCRIPCION as descripcion 
    from lalydia.ACTIVO_mejora mj
    inner join lalydia.ACTIVO_FIJO af on af.ACTIVO_FIJO = mj.ACTIVO_FIJO 
    where af.TIPO_ACTIVO in (3,5,12,13,14,32) and mj.ACTIVO_FIJO  = mj.MEJORA 
    ORDER BY mj.DESCRIPCION ASC`,
    { type: sequelize.QueryTypes.SELECT }
  );
};

ACTIVO_FIJO.obtenerEmpleadosMaquinaria = () => {
  return sequelize.query(
    `select EMPLEADO as value, CONCAT( NOMBRE,' -> (',EMPLEADO,')') as label  
    from lalydia.EMPLEADO e   
    where DEPARTAMENTO like 'OM' and ACTIVO LIKE 'S'
    order by NOMBRE `,
    { type: sequelize.QueryTypes.SELECT }
  );
};

return ACTIVO_FIJO;
};

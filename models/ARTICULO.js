"use strict";
module.exports = (sequelize, DataTypes) => {
  var ARTICULO = sequelize.define(
    'ARTICULO',
    {
      RowPointer: {
        type: DataTypes.STRING,
        autoIncrement: true,
        primaryKey: true,
      },
      ARTICULO: DataTypes.STRING,
      NoteExistsFlag: DataTypes.INTEGER,
      RecordDate: DataTypes.TIME,
      CreatedBy: DataTypes.STRING,
      UpdatedBy: DataTypes.STRING,
      CreateDate: DataTypes.TIME,
      DESCRIPCION: DataTypes.STRING,
      CLASIFICACION_1: DataTypes.STRING,
      CLASIFICACION_2: DataTypes.STRING,
      CLASIFICACION_3: DataTypes.STRING,
      FACTOR_CONVER_1: DataTypes.STRING,
      FACTOR_CONVER_2: DataTypes.STRING,
      FACTOR_CONVER_3: DataTypes.STRING,
      TIPO: DataTypes.STRING,
      ORIGEN_CORP: DataTypes.STRING,
      PESO_NETO: DataTypes.INTEGER,
      PESO_BRUTO: DataTypes.INTEGER,
      VOLUMEN: DataTypes.INTEGER,
      BULTOS: DataTypes.INTEGER,
      ARTICULO_CUENTA: DataTypes.STRING,
      IMPUESTO: DataTypes.STRING,
      UNIDAD_ALMACEN: DataTypes.STRING,
      UNIDAD_EMPAQUE: DataTypes.STRING,
      UNIDAD_VENTA: DataTypes.STRING,
      FACTOR_EMPAQUE: DataTypes.INTEGER,
      FACTOR_VENTA: DataTypes.INTEGER,
      EXISTENCIA_MINIMA: DataTypes.INTEGER,
      EXISTENCIA_MAXIMA: DataTypes.INTEGER,
      PUNTO_DE_REORDEN: DataTypes.INTEGER,
      COSTO_FISCAL: DataTypes.STRING,
      COSTO_COMPARATIVO: DataTypes.STRING,
      COSTO_PROM_LOC: DataTypes.INTEGER,
      COSTO_PROM_DOL: DataTypes.INTEGER,
      COSTO_STD_LOC: DataTypes.INTEGER,
      COSTO_STD_DOL: DataTypes.INTEGER,
      COSTO_ULT_LOC: DataTypes.INTEGER,
      COSTO_ULT_DOL: DataTypes.INTEGER,
      PRECIO_BASE_LOCAL: DataTypes.INTEGER,
      PRECIO_BASE_DOLAR: DataTypes.INTEGER,
      ULTIMA_SALIDA: DataTypes.TIME,
      ULTIMO_MOVIMIENTO: DataTypes.TIME,
      ULTIMO_INGRESO: DataTypes.TIME,
      ULTIMO_INVENTARIO: DataTypes.TIME,
      CLASE_ABC: DataTypes.STRING,
      FRECUENCIA_CONTEO: DataTypes.INTEGER,
      CODIGO_BARRAS_VENT: DataTypes.STRING,
      CODIGO_BARRAS_INVT: DataTypes.STRING,
      ACTIVO: DataTypes.STRING,
      USA_LOTES: DataTypes.STRING,
      OBLIGA_CUARENTENA: DataTypes.STRING,
      MIN_VIDA_COMPRA: DataTypes.INTEGER,
      MIN_VIDA_CONSUMO: DataTypes.INTEGER,
      MIN_VIDA_VENTA: DataTypes.INTEGER,
      VIDA_UTIL_PROM: DataTypes.INTEGER,
      DIAS_CUARENTENA: DataTypes.INTEGER,
      PROVEEDOR: DataTypes.STRING,
      ARTICULO_DEL_PROV: DataTypes.STRING,
      ORDEN_MINIMA: DataTypes.INTEGER,
      PLAZO_REABAST: DataTypes.INTEGER,
      LOTE_MULTIPLO: DataTypes.INTEGER,
      NOTAS: DataTypes.STRING,
      UTILIZADO_MANUFACT: DataTypes.STRING,
      USUARIO_CREACION: DataTypes.STRING,
      FCH_HORA_CREACION: DataTypes.TIME,
      USUARIO_ULT_MODIF: DataTypes.TIME,
      FCH_HORA_ULT_MODIF: DataTypes.TIME,
      USA_NUMEROS_SERIE: DataTypes.STRING,
      MODALIDAD_INV_FIS: DataTypes.STRING,
      PLANTILLA_SERIE: DataTypes.STRING,
      TIPO_COD_BARRA_DET: DataTypes.STRING,
      TIPO_COD_BARRA_ALM: DataTypes.STRING,
      USA_REGLAS_LOCALES: DataTypes.STRING,
      CLASIFICACION_4: DataTypes.STRING,
      CLASIFICACION_5: DataTypes.STRING,
      CLASIFICACION_6: DataTypes.STRING,
      FACTOR_CONVER_4: DataTypes.INTEGER,
      FACTOR_CONVER_5: DataTypes.INTEGER,
      FACTOR_CONVER_6: DataTypes.INTEGER,
      PERECEDERO: DataTypes.STRING,
      GTIN: DataTypes.STRING,
      MANUFACTURADOR: DataTypes.STRING,
      CODIGO_RETENCION: DataTypes.STRING,
      RETENCION_VENTA: DataTypes.STRING,
      RETENCION_COMPRA: DataTypes.STRING,
      MODELO_RETENCION: DataTypes.STRING,
      ESTILO: DataTypes.STRING,
      TALLA: DataTypes.STRING,
      COLOR: DataTypes.STRING,
      TIPO_COSTO: DataTypes.STRING,
      ARTICULO_ENVASE: DataTypes.STRING,
      ES_ENVASE: DataTypes.STRING,
      USA_CONTROL_ENVASE: DataTypes.STRING,
      COSTO_PROM_COMPARATIVO_LOC: DataTypes.INTEGER,
      COSTO_PROM_COMPARATIVO_DOLAR: DataTypes.INTEGER,
      COSTO_PROM_ULTIMO_LOC: DataTypes.INTEGER,
      COSTO_PROM_ULTIMO_DOL: DataTypes.INTEGER,
      UTILIZADO_EN_CONTRATOS: DataTypes.STRING,
      VALIDA_CANT_FASE_PY: DataTypes.STRING,
      OBLIGA_INCLUIR_FASE_PY: DataTypes.STRING,
      ES_IMPUESTO: DataTypes.STRING,
      U_CLAVE_UNIDAD: DataTypes.STRING,
      U_CLAVE_PROD_SERV: DataTypes.STRING,
      U_CLAVE_PS_PUB: DataTypes.STRING,
      TIPO_DOC_IVA: DataTypes.STRING,
      NIT: DataTypes.STRING,
      CANASTA_BASICA: DataTypes.STRING,
      ES_OTRO_CARGO: DataTypes.STRING,
      SERVICIO_MEDICO: DataTypes.STRING,
      item_hacienda: DataTypes.STRING,
      CODIGO_HACIENDA: DataTypes.STRING,
      ITEM_HACIENDA_COMPRA: DataTypes.STRING,
      TIENDA: DataTypes.STRING,
      SUGIERE_MIN: DataTypes.STRING,
      TIPO_EXISTENCIA: DataTypes.STRING,
      CATALOGO_EXISTENCIA: DataTypes.STRING,
      TIPO_DETRACCION_VENTA: DataTypes.STRING,
      CODIGO_DETRACCION_VENTA: DataTypes.STRING,
      TIPO_DETRACCION_COMPRA: DataTypes.STRING,
      CODIGO_DETRACCION_COMPRA: DataTypes.STRING,
      CALC_PERCEP: DataTypes.STRING,
      PORC_PERCEP: DataTypes.INTEGER,
    },
    {
      timestamps: false,
      hasTrigger: true,
      freezeTableName: true,
      tableName: "ARTICULO",
    }
  );


  ARTICULO.obtenerArticulos = function (articulosId) {
    return sequelize.query(
        `select a.ARTICULO,a.DESCRIPCION,eb.CANT_DISPONIBLE from lalydia.ARTICULO a 
        inner join lalydia.EXISTENCIA_BODEGA eb on eb.ARTICULO = a.ARTICULO  where a.ARTICULO in (${articulosId}) and eb.BODEGA LIKE '08' `,
      { type: sequelize.QueryTypes.SELECT }
    );
  };

  ARTICULO.articulosExactus = function () {
    return sequelize.query(
        `select a.ARTICULO, a.DESCRIPCION, a.FCH_HORA_CREACION, a.UNIDAD_EMPAQUE, eb.CANT_DISPONIBLE from  lalydia.ARTICULO a 
        inner join lalydia.EXISTENCIA_BODEGA eb on eb.ARTICULO = a.ARTICULO  where  eb.BODEGA LIKE '08' order by a.FCH_HORA_CREACION asc`,
      { type: sequelize.QueryTypes.SELECT }
    );
  };


  ARTICULO.obtenerOrdenesCompra = function (fechaI,fechaF){
    return sequelize.query(
      `select * from lalydia.ORDEN_COMPRA oc 
       inner join lalydia.ORDEN_COMPRA_LINEA ocl on oc.ORDEN_COMPRA  = ocl.ORDEN_COMPRA
       where oc.FECHA_HORA_CONFIR  between '${fechaI}' and '${fechaF}' and oc.BODEGA LIKE '08'`,
       { type: sequelize.QueryTypes.SELECT }
    )

  }

  ARTICULO.obtenerCostoArticulo = function (articulo){
    return sequelize.query(
      `select top 1 COSTO_PROM_LOC , COSTO_PROM_DOL from lalydia.ARTICULO a where ARTICULO like '${articulo}' order by FCH_HORA_CREACION desc`,
       { type: sequelize.QueryTypes.SELECT }
    )

  }

  // Obtener los centros costos software de maderas
  ARTICULO.obtenerCentroCostos = function (){
    return sequelize.query(
      `select CENTRO_COSTO as value,CONCAT(CENTRO_COSTO, ' -> ', DESCRIPCION )   as label from lalydia.CENTRO_COSTO cc 
      where  ACEPTA_DATOS like 'S'
      order by DESCRIPCION`,
       { type: sequelize.QueryTypes.SELECT }
    )
  }

  ARTICULO.obtenerLabores = function (){
    return sequelize.query(
      `select  CONCEPTO as id,CONCEPTO as value, CONCAT(CONCEPTO,' -> ', DESCRIPCION,' -> ',UNIDADES ) as label,CONCAT(CONCEPTO,' -> ', DESCRIPCION,' -> ',UNIDADES ) as name,UNIDADES as unidades,DESCRIPCION as descripcion
      from lalydia.CONCEPTO c
      where TIPO_CONCEPTO like 'B' or CONCEPTO like 'HAUSEN' or CONCEPTO like 'HAUSENCP'
     order by DESCRIPCION`,
       { type: sequelize.QueryTypes.SELECT }
    )
  }

  ARTICULO.obtenerEmpleados = function (){
    return sequelize.query(
      `select EMPLEADO as value, CONCAT(EMPLEADO,' -> ', NOMBRE )   as label from lalydia.EMPLEADO e where ACTIVO like 'S'
      order by NOMBRE`,
       { type: sequelize.QueryTypes.SELECT }
    )
  }

  ARTICULO.obtenerEmpleadoPorCodigo = function (codigo){
    return sequelize.query(
      `select NOMBRE as nombre, EMPLEADO as codigo,IDENTIFICACION as identificacion,SEXO as sexo, TELEFONO1 as telefono1 
      from lalydia.EMPLEADO e where EMPLEADO like '${codigo}' `,
       { type: sequelize.QueryTypes.SELECT }
    )
  }

  ARTICULO.obtenerInsumos = function (){
    return sequelize.query(
      `select a.ARTICULO as value, concat(a.ARTICULO ,' -> ',a.DESCRIPCION,' -> ',a.UNIDAD_EMPAQUE ) as label from  lalydia.ARTICULO a 
      inner join lalydia.EXISTENCIA_BODEGA eb on eb.ARTICULO = a.ARTICULO  
      where  eb.BODEGA LIKE '08'
      or  a.ARTICULO in ('010402','72646','010059','010114', '73233') 
      GROUP by a.ARTICULO,a.DESCRIPCION,a.UNIDAD_EMPAQUE
      order by a.DESCRIPCION asc`,
       { type: sequelize.QueryTypes.SELECT }
    )
  }

  ARTICULO.obtenerInsumosGanadero = function (){
    return sequelize.query(
     `select a.ARTICULO as value, concat(a.ARTICULO ,' -> ',a.DESCRIPCION,' -> ',a.UNIDAD_EMPAQUE ) as label from  lalydia.ARTICULO a 
      inner join lalydia.EXISTENCIA_BODEGA eb on eb.ARTICULO = a.ARTICULO  
      where  eb.BODEGA LIKE '02' or eb.BODEGA LIKE '09'
      order by a.DESCRIPCION asc`,
      { type: sequelize.QueryTypes.SELECT }
    )
  }

  ARTICULO.obtenerInsumoPorCodigo = function (codigo){
    return sequelize.query(
      `select a.ARTICULO as codigo,a.DESCRIPCION as descripcion, a.COSTO_ULT_LOC as costo from  lalydia.ARTICULO a 
      where  a.ARTICULO like '${codigo}'`,
       { type: sequelize.QueryTypes.SELECT }
    )
  }

   //Obtiene las bodegas que esta un insumo
   ARTICULO.obtenerBodegasInsumos = function (codigo){
    return sequelize.query(
      `select b.BODEGA as value, b.NOMBRE as label from  lalydia.ARTICULO a 
      inner join lalydia.EXISTENCIA_BODEGA eb on eb.ARTICULO = a.ARTICULO  
      inner join lalydia.BODEGA b on b.BODEGA = eb.BODEGA 
      where  (eb.BODEGA LIKE '02' or eb.BODEGA LIKE '09') and a.ARTICULO like '${codigo}'
      order by a.DESCRIPCION asc
      `,
       { type: sequelize.QueryTypes.SELECT }
    )
  }

  // Obtiene centro costo planilla
  ARTICULO.obtenerCentroCostoPlanillas = function (){
    return sequelize.query(
      `select CENTRO_COSTO as id,CONCAT(CENTRO_COSTO, ' -> ', DESCRIPCION ) as name,DESCRIPCION as descripcion from lalydia.CENTRO_COSTO cc 
      where ACEPTA_DATOS like 'S' and CENTRO_COSTO NOT LIKE '08-01%' AND CENTRO_COSTO NOT LIKE '05-01%' AND CENTRO_COSTO NOT LIKE '05-03%'  
      order by DESCRIPCION`,
       { type: sequelize.QueryTypes.SELECT }
    )
  }

  ARTICULO.obtenerLaboresPitalia = function (){
    return sequelize.query(
      `select  CONCEPTO as id,CONCEPTO as value, CONCAT(CONCEPTO,' -> ', DESCRIPCION,' (PITALIA)', ' -> ',UNIDADES ) as label,
      CONCAT(CONCEPTO,' -> ', DESCRIPCION,' (PITALIA)', ' -> ',UNIDADES ) as name,UNIDADES as unidades,DESCRIPCION as descripcion
             from PITALIA.CONCEPTO c
             where TIPO_CONCEPTO like 'B' or CONCEPTO like 'HAUSEN' or CONCEPTO like 'HAUSENCP'
            order by DESCRIPCION`,
       { type: sequelize.QueryTypes.SELECT }
    )
  }

  ARTICULO.obtenerCentroCostoPlanillasPitalia = function (){
    return sequelize.query(
      `select CENTRO_COSTO as id,CONCAT(CENTRO_COSTO, ' -> ', DESCRIPCION,' (PITALIA)' ) as name,DESCRIPCION as descripcion from PITALIA.CENTRO_COSTO cc 
      where ACEPTA_DATOS like 'S'
      order by DESCRIPCION`,
       { type: sequelize.QueryTypes.SELECT }
    )
  }

  ARTICULO.obtenerProyectos = function (){
    return sequelize.query(
      `select py.PROYECTO AS id, py.DESCRIPCION as name, 
      py.PROYECTO as proyecto, py.DESCRIPCION as descripcion_proyecto,
      cc.CENTRO_COSTO as centro_costo, cc.DESCRIPCION as descripcion_cc
      from lalydia.PROYECTO_PY py
      inner join lalydia.CENTRO_COSTO cc on cc.CENTRO_COSTO = py.CENTRO_COSTO
     -- and PROYECTO not like 'REFORESTACION%' 
     -- and PROYECTO not like 'CULTIVO%'
      where PROYECTO not like 'PLANTILLA%' 
      AND ESTADO like 'A'
      order by py.PROYECTO, py.DESCRIPCION`,
       { type: sequelize.QueryTypes.SELECT }
    )
  }

  ARTICULO.obtenerFases = function (){
    return sequelize.query(
      `select PROYECTO as proyecto, FASE AS id, NOMBRE as name, 
      FASE AS fase, NOMBRE as descripcion_fase from lalydia.FASE_PY
      where ACEPTA_DATOS like 'S' AND TIPO like 'M'
      and PROYECTO not like 'PLANTILLA%' 
     -- and PROYECTO not like 'REFORESTACION%' 
     -- and PROYECTO not like 'CULTIVO%'
      order by PROYECTO, FASE`,
       { type: sequelize.QueryTypes.SELECT }
    )
  }

  ARTICULO.obtenerProyectosMadera = function (){
    return sequelize.query(
      `select PROYECTO as id, DESCRIPCION as name,
      PROYECTO as value, DESCRIPCION as label,
      PROYECTO as proyecto, DESCRIPCION as descripcion_proyecto
     from lalydia.PROYECTO_PY where (
      PROYECTO LIKE 'CULTIVO%' OR
      PROYECTO LIKE 'HENO%' OR
      PROYECTO LIKE 'FORRAJE%' OR
      PROYECTO LIKE 'REFORESTACION%') 
      order by PROYECTO`,
       { type: sequelize.QueryTypes.SELECT }
    )
  }

  ARTICULO.obtenerFasesMadera = function (){
    return sequelize.query(
      `select FASE as id, CONCAT(FASE , ' -> ', NOMBRE) as name,
      FASE as fase, NOMBRE as descripcion_fase, PROYECTO
      from lalydia.FASE_PY
      where ACEPTA_DATOS like 'S' and TIPO like 'M'
      and (
      PROYECTO LIKE 'CULTIVO%' OR
      PROYECTO LIKE 'HENO%' OR
      PROYECTO LIKE 'FORRAJE%' OR
      PROYECTO LIKE 'REFORESTACION%') 
       or (FASE LIKE '05-01-00' and  PROYECTO LIKE 'REFORESTACION-GENERAL%')
      order by PROYECTO,FASE, NOMBRE`,
       { type: sequelize.QueryTypes.SELECT }
    )
  }

  ARTICULO.obtenerFasesMaderaArticulos = function (){
    return sequelize.query(
      `select FASE as value, CONCAT(FASE , ' -> ', NOMBRE) as label,
      FASE as fase, NOMBRE as descripcion_fase
      from lalydia.FASE_PY
      where ACEPTA_DATOS like 'S' and TIPO like 'A'
      and (
      PROYECTO LIKE 'CULTIVO%' OR
      PROYECTO LIKE 'HENO%' OR
      PROYECTO LIKE 'REFORESTACION%')
      group by FASE, NOMBRE
      order by FASE, NOMBRE`,
       { type: sequelize.QueryTypes.SELECT }
    )
  }

  /**
   * Obtener Fases eAgro
   */
  ARTICULO.obtenerFaseseAgro = function (){
    return sequelize.query(
      ` select fase as value, NOMBRE as label,TIPO  from lalydia.FASE_PY fp where PROYECTO LIKE '%PIÑA_LOTE29%' and ACEPTA_DATOS like 'S' and FASE in (
      --PRIMERAS
      '1-01-01-02','1-02-03-02','1-03-01-02','1-03-02-02','1-03-03-02','1-03-04-02','1-03-05-02','1-03-06-02','1-03-08-02','1-03-09-02',
      --SEGUNDAS
      '2-01-01-02','2-02-03-02','2-03-01-02','2-03-02-02','2-03-03-02','2-03-04-02','2-03-05-02','2-03-06-02','2-03-08-02','2-03-09-02',
     --SEMILLEROS
	   '4-01-02-00','4-02-02-00','4-03-02-00','4-04-02-00')
	    ORDER BY NOMBRE `,
       { type: sequelize.QueryTypes.SELECT }
    )
  }


  return ARTICULO;
};

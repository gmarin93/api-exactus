"use strict";
module.exports = (sequelize, DataTypes) => {
  var BODEGA = sequelize.define('BODEGA',
    {
      BODEGA: DataTypes.INTEGER,
      NOMBRE: DataTypes.STRING,
      
    },
    {
      timestamps: false,
      hasTrigger: true,
      freezeTableName: true,
      tableName: "BODEGA",
    }
  );


  return BODEGA;
};

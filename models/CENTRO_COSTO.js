'use strict';
module.exports = (sequelize, DataTypes) => {
var CENTRO_COSTO = sequelize.define('CENTRO_COSTO', {
  // RowPointer: { type: DataTypes.STRING, primaryKey: true }, 
  // CENTRO_COSTO	: DataTypes.STRING,
  // UBICACION	: DataTypes.STRING,
  // TIPO_ACTIVO	: DataTypes.STRING,
  // DESCRIPCION	: DataTypes.STRING,
  // FECHA_ULT_MANT	: DataTypes.TIME,
  // FECHA_PROX_MANT	: DataTypes.TIME,
  // ESTADO_ACTIVO	: DataTypes.STRING,
  // TASA_DEPRECIACION_F	: DataTypes.DECIMAL,
  // TASA_DEPRECIACION_C	: DataTypes.DECIMAL,
  // CLASIFICACION	: DataTypes.STRING,
  // USA_COMPONENTE	: DataTypes.STRING,
  // NoteExistsFlag	: DataTypes.INTEGER,
  // RecordDate	: DataTypes.TIME,
  // CreatedBy	: DataTypes.STRING,
  // UpdatedBy	: DataTypes.STRING,
  // CreateDate	: DataTypes.TIME,
  
}, {
 timestamps: false,
 hasTrigger: true,
 freezeTableName: true,
 tableName: 'CENTRO_COSTO'
});


CENTRO_COSTO.obtenerCentrosCostoGanadero = () => {
  return sequelize.query(
    `select pp.PROYECTO as value, pp.DESCRIPCION as label, cc.CENTRO_COSTO as centro_costo, cc.DESCRIPCION as descripcion_cc 
    from lalydia.PROYECTO_PY pp 
    inner join lalydia.CENTRO_COSTO cc on cc.CENTRO_COSTO = pp.CENTRO_COSTO
    where pp.PROYECTO like 'GANADO%'
    order by pp.PROYECTO `,
    { type: sequelize.QueryTypes.SELECT }
  );
};

return CENTRO_COSTO;
};

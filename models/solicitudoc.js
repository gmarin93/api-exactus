'use strict';
module.exports = (sequelize, DataTypes) => {
var SOLICITUD_OC = sequelize.define('SOLICITUD_OC', {
  RowPointer: { type: DataTypes.STRING, primaryKey: true },
  SOLICITUD_OC: DataTypes.STRING,
  DEPARTAMENTO: DataTypes.STRING,
  FECHA_SOLICITUD: DataTypes.TIME,
  FECHA_REQUERIDA: DataTypes.TIME,
  AUTORIZADA_POR: DataTypes.STRING,
  COMENTARIO: DataTypes.STRING,
  USUARIO_CANCELA: DataTypes.STRING,
  FECHA_HORA_CANCELA: DataTypes.TIME,
  RecordDate: DataTypes.TIME,
  LINEAS_NO_ASIG:DataTypes.INTEGER,
  PRIORIDAD: DataTypes.STRING,
  ESTADO: DataTypes.STRING,
  USUARIO: DataTypes.STRING,
  UpdatedBy: DataTypes.STRING,
  CreateDate: DataTypes.TIME,
  FECHA_HORA: DataTypes.TIME,
}, {
 timestamps: false,
 hasTrigger: true,
 freezeTableName: true,
 tableName: 'SOLICITUD_OC'
});

SOLICITUD_OC.ultimoIdSolicitudOc= function(){
    
    return sequelize.query(
     `select top 1 SOLICITUD_OC from lalydia.SOLICITUD_OC ORDER BY SOLICITUD_OC desc`,
     { type: sequelize.QueryTypes.SELECT}
    );
  };
  
  SOLICITUD_OC.validarId= function(id){
      
    return sequelize.query(
     `Select * from lalydia.SOLICITUD_OC where SOLICITUD_OC  like '${id}'`,
     { type: sequelize.QueryTypes.SELECT}
    );
  };
  
  
  SOLICITUD_OC.associate = function(models) {
    
    SOLICITUD_OC.hasMany(models.SOLICITUD_OC_LINEA, {   
    foreignKey:'SOLICITUD_OC'
   });

  //  DOCUMENTO_INV.belongsTo(models.prd_lotes,{
  //   foreignKey:'loteid'
  // });

};


return SOLICITUD_OC;
};

"use strict";
module.exports = (sequelize, DataTypes) => {
  var PEDIDO = sequelize.define(
    "PEDIDO",
    {
      RowPointer: { type: DataTypes.STRING, primaryKey: true },
      ACTIVIDAD_COMERCIAL: DataTypes.STRING,
      AUTORIZADO: DataTypes.STRING,
      BACKORDER: DataTypes.STRING,
      BASE_IMPUESTO1: DataTypes.DECIMAL,
      BASE_IMPUESTO2: DataTypes.DECIMAL,
      BODEGA: DataTypes.STRING,
      CAMBIOS_COTI: DataTypes.STRING,
      CLASE_PEDIDO: DataTypes.STRING,
      CLAVE_REFERENCIA_DE: DataTypes.STRING,
      CLIENTE: DataTypes.STRING,
      CLIENTE_CORPORAC: DataTypes.STRING,
      CLIENTE_DIRECCION: DataTypes.STRING,
      CLIENTE_ORIGEN: DataTypes.STRING,
      COBRADOR: DataTypes.STRING,
      CODIGO_REFERENCIA_DE: DataTypes.STRING,
      COMENTARIO_CXC: DataTypes.STRING,
      CONDICION_PAGO: DataTypes.STRING,
      CONSECUTIVO_FTC: DataTypes.STRING,
      CONTRATO: DataTypes.STRING,
      CONTRATO_AC: DataTypes.STRING,
      CONTRATO_REVENTA: DataTypes.STRING,
      CONTRATO_VIGENCIA_DESDE: DataTypes.TIME,
      CONTRATO_VIGENCIA_HASTA: DataTypes.TIME,
      CORREOS_ENVIO: DataTypes.STRING,
      COTIZACION_PADRE: DataTypes.STRING,
      CreateDate: DataTypes.TIME,
      CreatedBy: DataTypes.STRING,
      DES_CANCELA_COTI: DataTypes.STRING,
      DESC_DIREC_EMBARQUE: DataTypes.STRING,
      DESCUENTO_CASCADA: DataTypes.STRING,
      DESCUENTO_VOLUMEN: DataTypes.DECIMAL,
      DIFERIDO_CONTRATO_AC: DataTypes.STRING,
      DIREC_EMBARQUE: DataTypes.STRING,
      DIRECCION_FACTURA: DataTypes.STRING,
      DIVISION_GEOGRAFICA1: DataTypes.STRING,
      DIVISION_GEOGRAFICA2: DataTypes.STRING,
      DOC_A_GENERAR: DataTypes.STRING,
      EMBARCAR_A: DataTypes.STRING,
      ES_FACTURA_REEMPLAZO: DataTypes.STRING,
      ESTADO: DataTypes.STRING,
      FACTURA_ORIGINAL_REEMPLAZO: DataTypes.STRING,
      FECHA_APROBACION: DataTypes.TIME,
      FECHA_CONTRATO_AC: DataTypes.TIME,
      FECHA_FINFAC_CONTRATO_AC: DataTypes.TIME,
      FECHA_HORA: DataTypes.TIME,
      FECHA_INICIO_CONTRATO_AC: DataTypes.TIME,
      FECHA_NO_APRUEBA: DataTypes.TIME,
      FECHA_ORDEN: DataTypes.TIME,
      FECHA_PEDIDO: DataTypes.TIME,
      FECHA_PROMETIDA: DataTypes.TIME,
      FECHA_PROX_EMBARQU: DataTypes.TIME,
      FECHA_PROXFAC_CONTRATO_AC: DataTypes.TIME,
      FECHA_PROXFACSIST_CONTRATO_AC: DataTypes.TIME,
      FECHA_PROYECTADA: DataTypes.TIME,
      FECHA_REFERENCIA_DE: DataTypes.TIME,
      FECHA_ULT_CANCELAC: DataTypes.TIME,
      FECHA_ULT_EMBARQUE: DataTypes.TIME,
      FECHA_ULTAUMENTO_CONTRATO_AC: DataTypes.TIME,
      FIJAR_TIPO_CAMBIO: DataTypes.STRING,
      FORMA_PAGO: DataTypes.STRING,
      IMPRESO: DataTypes.STRING,
      INCOTERMS: DataTypes.STRING,
      MODULO: DataTypes.STRING,
      MONEDA: DataTypes.STRING,
      MONEDA_PEDIDO: DataTypes.STRING,
      MONTO_ANTICIPO: DataTypes.DECIMAL,
      MONTO_DESCUENTO1: DataTypes.DECIMAL,
      MONTO_DESCUENTO2: DataTypes.DECIMAL,
      MONTO_DOCUMENTACIO: DataTypes.DECIMAL,
      MONTO_FLETE: DataTypes.DECIMAL,
      MONTO_OTRO_CARGO: DataTypes.DECIMAL,
      MONTO_SEGURO: DataTypes.DECIMAL,
      NIT_TRANSPORTADOR: DataTypes.STRING,
      NIVEL_PRECIO: DataTypes.STRING,
      NOMBRE_CLIENTE: DataTypes.STRING,
      NoteExistsFlag: DataTypes.INTEGER,
      NUM_CONS_REG_EXO: DataTypes.STRING,
      NUM_IRSEDE_AGR_GAN: DataTypes.STRING,
      NUM_OC_EXENTA: DataTypes.STRING,
      NUMERO_FTC: DataTypes.STRING,
      OBSERVACIONES: DataTypes.STRING,
      ORDEN_COMPRA: DataTypes.STRING,
      ORIGEN_PEDIDO: DataTypes.STRING,
      PAIS: DataTypes.STRING,
      PEDIDO: DataTypes.STRING,
      PERIODICIDAD_CONTRATO_AC: DataTypes.STRING,
      PORC_COMI_COBRADOR: DataTypes.DECIMAL,
      PORC_COMI_VENDEDOR: DataTypes.DECIMAL,
      PORC_DESCUENTO1: DataTypes.DECIMAL,
      PORC_DESCUENTO2: DataTypes.DECIMAL,
      PORC_INTCTE: DataTypes.DECIMAL,
      RAZON_CANCELA_COTI: DataTypes.STRING,
      RAZON_DESAPRUEBA: DataTypes.STRING,
      RecordDate: DataTypes.TIME,
      RUBRO1: DataTypes.STRING,
      RUBRO2: DataTypes.STRING,
      RUBRO3: DataTypes.STRING,
      RUBRO4: DataTypes.STRING,
      RUBRO5: DataTypes.STRING,
      RUTA: DataTypes.STRING,
      SUBTIPO_DOC_CXC: DataTypes.INTEGER,
      TARJETA_CREDITO: DataTypes.STRING,
      TASA_CREE1: DataTypes.STRING,
      TASA_CREE1_PORC: DataTypes.DECIMAL,
      TASA_CREE2: DataTypes.STRING,
      TASA_CREE2_PORC: DataTypes.DECIMAL,
      TASA_GAN_OCASIONAL_PORC: DataTypes.DECIMAL,
      TASA_IMPOSITIVA: DataTypes.STRING,
      TASA_IMPOSITIVA_PORC: DataTypes.DECIMAL,
      TIENE_RELACIONADOS: DataTypes.STRING,
      TIPO_CAMBIO: DataTypes.DECIMAL,
      TIPO_CONTRATO_AC: DataTypes.STRING,
      TIPO_DESCUENTO_GLOBAL: DataTypes.STRING,
      TIPO_DESCUENTO1: DataTypes.STRING,
      TIPO_DESCUENTO2: DataTypes.STRING,
      TIPO_DOC_CXC: DataTypes.STRING,
      TIPO_DOCUMENTO: DataTypes.STRING,
      TIPO_FACTURA: DataTypes.STRING,
      TIPO_OPERACION: DataTypes.STRING,
      TIPO_PAGO: DataTypes.STRING,
      TIPO_PEDIDO: DataTypes.STRING,
      TIPO_REFERENCIA_DE: DataTypes.STRING,
      TOTAL_A_FACTURAR: DataTypes.DECIMAL,
      TOTAL_CANCELADO: DataTypes.DECIMAL,
      TOTAL_CONTRATO_AC: DataTypes.DECIMAL,
      TOTAL_IMPUESTO1: DataTypes.DECIMAL,
      TOTAL_IMPUESTO2: DataTypes.DECIMAL,
      TOTAL_MERCADERIA: DataTypes.DECIMAL,
      TOTAL_UNIDADES: DataTypes.DECIMAL,
      U_AD_AM_ENVIAR_GLN: DataTypes.STRING,
      U_AD_AM_FECHA_RECEPCION: DataTypes.STRING,
      U_AD_AM_FECHA_RECLAMO: DataTypes.STRING,
      U_AD_AM_NUMERO_PROVEEDOR: DataTypes.STRING,
      U_AD_AM_NUMERO_RECEPCION: DataTypes.STRING,
      U_AD_AM_NUMERO_RECLAMO: DataTypes.STRING,
      U_AD_CC_FECHA_CONSUMO: DataTypes.DATE,
      U_AD_CC_HOJA_ENTRADA: DataTypes.STRING,
      U_AD_CC_REMISION: DataTypes.STRING,
      U_AD_ENVIAR_GLN: DataTypes.STRING,
      U_AD_GS_ENVIAR_GLN: DataTypes.STRING,
      U_AD_GS_FECHA_RECEPCION: DataTypes.STRING,
      U_AD_GS_NUMERO_RECEPCION: DataTypes.STRING,
      U_AD_GS_NUMERO_VENDEDOR: DataTypes.STRING,
      U_AD_NUMERO_RECLAMO: DataTypes.STRING,
      U_AD_PC_ENVIAR_GLN: DataTypes.STRING,
      U_AD_PC_NUMERO_VENDEDOR: DataTypes.STRING,
      U_AD_WM_ENVIAR_GLN: DataTypes.STRING,
      U_AD_WM_FECHA_RECLAMO: DataTypes.STRING,
      U_AD_WM_NUMERO_RECEPCION: DataTypes.STRING,
      U_AD_WM_NUMERO_RECLAMO: DataTypes.STRING,
      U_AD_WM_NUMERO_VENDEDOR: DataTypes.STRING,
      UpdatedBy: DataTypes.STRING,
      USO_CFDI: DataTypes.STRING,
      USR_NO_APRUEBA: DataTypes.STRING,
      USUARIO: DataTypes.STRING,
      VENDEDOR: DataTypes.STRING,
      VERSION_COTIZACION: DataTypes.STRING,
      VERSION_NP: DataTypes.INTEGER,
      ZONA: DataTypes.STRING,
    },
    {
      timestamps: false,
      hasTrigger: true,
      freezeTableName: true,
      tableName: "PEDIDO",
    }
  );

  PEDIDO.associate = function (models) {
    PEDIDO.hasMany(models.PEDIDO_LINEA, {
      foreignKey: "PEDIDO",
    });
  };

  //Obtiene el ultimo id Pedido
  PEDIDO.ultimoIdPedido = () => {
    return sequelize.query(
      `select VALOR_CONSECUTIVO  AS PEDIDO from lalydia.CONSECUTIVO_FA  where CODIGO_CONSECUTIVO LIKE  'PREFACT'`,
      { type: sequelize.QueryTypes.SELECT }
    );
  };

  //Obtiene la version del precio
  PEDIDO.versionPrecio = (nivel_precio) => {
    return sequelize.query(
      `select top 1 VERSION from lalydia.VERSION_NIVEL vn
        where NIVEL_PRECIO like '${nivel_precio}' and ESTADO like 'A'  order by VERSION desc`,
      { type: sequelize.QueryTypes.SELECT }
    );
  };

  //Obtiene la version de la semana que se facturo
  PEDIDO.versionPrecioPorSemana = (cliente, fInicio, fFin) => {
    return sequelize.query(
      ` select top 1 p.VERSION_NP as VERSION from lalydia.PEDIDO p where FECHA_PEDIDO 
           BETWEEN '${fInicio}' and '${fFin}' and CLIENTE = ${cliente} GROUP by p.VERSION_NP`,
      { type: sequelize.QueryTypes.SELECT }
    );
  };

  //Obtiene la version del precio
  PEDIDO.obtieneArticulosPorCliente = (nivel_precio, version) => {
    return sequelize.query(
      `SELECT ap.ARTICULO ,ap.PRECIO, ap.VERSION, a.CANASTA_BASICA, i.IMPUESTO1, i.TIPO_TARIFA1  from lalydia.ARTICULO_PRECIO ap
       inner join lalydia.ARTICULO a on a.ARTICULO = ap.ARTICULO 
       inner join lalydia.IMPUESTO i on a.IMPUESTO = i.IMPUESTO 
       where ap.NIVEL_PRECIO like '${nivel_precio}' and ap.VERSION like '${version}'
       order by ap.ARTICULO asc`,
      { type: sequelize.QueryTypes.SELECT }
    );
  };

  //Obtiene informacion del cliente
  PEDIDO.infoCliente = (cliente) => {
    return sequelize.query(
      `SELECT c.LOCAL, c.AFECTACION_IVA ,c.CLIENTE,c.VENDEDOR , c.COBRADOR ,c.PAIS ,c.RUTA , c.ZONA , c.CONDICION_PAGO , c.NOMBRE , c.DESCUENTO,c.TIPO_IMPUESTO ,c.DIRECCION , 
      c.DIVISION_GEOGRAFICA1 , c.DIVISION_GEOGRAFICA2,c.NIVEL_PRECIO, p.MONEDA  from lalydia.CLIENTE c 
      inner join lalydia.NIVEL_PRECIO p on p.NIVEL_PRECIO = c.NIVEL_PRECIO
      where  c.CLIENTE LIKE '${cliente}'`,
      { type: sequelize.QueryTypes.SELECT }
    );
  };

    //Obtiene exoneracion del cliente
    PEDIDO.getExoneracion = (cliente) => {
      return sequelize.query(
        `select CODIGO , CLIENTE  from lalydia.AUTOR_VENTA where CLIENTE like '${cliente}'`,
        { type: sequelize.QueryTypes.SELECT }
      );
    };

    // verifica si el articulo tiene exoneracion
    PEDIDO.getExoneracionArticulo = (codigo, articulo) => {
      return sequelize.query(
        `select CODIGO_ARTICULO  from lalydia.CLASIFICACION_VENTA cv where CODIGO like '${codigo}' and CODIGO_ARTICULO like '${articulo}'`,
        { type: sequelize.QueryTypes.SELECT }
      );
    };

    // verifica si el articulo tiene exoneracion
    PEDIDO.getDireccionEmbarque = (cliente) => {
      return sequelize.query(
        `select DIRECCION from lalydia.DIRECC_EMBARQUE de where CLIENTE like '${cliente}'`,
        { type: sequelize.QueryTypes.SELECT }
      );
    };

    // obtiene los clientes
    PEDIDO.getClientes = () => {
      return sequelize.query(
        `select CLIENTE AS value, CONCAT(CLIENTE , ' - ', NOMBRE ) AS label  from lalydia.CLIENTE c order by NOMBRE asc`,
        { type: sequelize.QueryTypes.SELECT }
      );
    };

    // Consultar el estado del pedido
    PEDIDO.getEstadoPedido = (pedido) => {
      return sequelize.query(
        `select ESTADO   from lalydia.PEDIDO where PEDIDO like '${pedido}'`,
        { type: sequelize.QueryTypes.SELECT }
      );
    };

    // Consultar el estado del pedido
    PEDIDO.getFacturas = (arreglo) => {
      return sequelize.query(
        `select PEDIDO , ESTADO from lalydia.PEDIDO p  where PEDIDO in (${arreglo})`,
        { type: sequelize.QueryTypes.SELECT }
      );
    };

  return PEDIDO;
};

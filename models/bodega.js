'use strict';
module.exports = (sequelize, DataTypes) => {
var bodega = sequelize.define('bodegas', {
  NOMBRE: DataTypes.STRING,
  TIPO: DataTypes.STRING,
  TELEFONO: DataTypes.STRING,
  DIRECCION: DataTypes.STRING,
  RowPointer: DataTypes.STRING,
  CONSEC_TRASLADOS: DataTypes.STRING,
  U_SUCURSAL: DataTypes.STRING,
  U_COORDINADAS:DataTypes.STRING,
  RecordDate:DataTypes.TIME,
  CreatedBy: DataTypes.STRING,
  UpdatedBy: DataTypes.STRING,
  CreateDate: DataTypes.TIME
}, {
 timestamps: true,
 freezeTableName: true,
 tableName: 'BODEGA'
});



bodega.salariosEtraordinarios= function () {
 return sequelize.query(

   'DECLARE @NOM AS VARCHAR(4)'+
   'SET @NOM =   '+"'"+1025+"'"+
    ' SELECT A.NUMERO_NOMINA AS NOMINA, A.EMPLEADO,D.NOMBRE,D.DEPARTAMENTO AS DEPTO,E.DESCRIPCION AS DEPARTAMENTO,A.CONCEPTO,C.DESCRIPCION,A.TOTAL,A.CANTIDAD,'+"'Extraordinario'"+' AS TIPO' +
    ' FROM EXACTUS.lalydia.EMPLEADO_CONC_NOMI A INNER JOIN Exactus.lalydia.NOMINA_HISTORICO B '+
    ' ON A.NUMERO_NOMINA = B.NUMERO_NOMINA AND A.NOMINA = B.NOMINA INNER JOIN EXACTUS.lalydia.CONCEPTO C ON A.CONCEPTO = C.CONCEPTO INNER JOIN ' +
    ' EXACTUS.lalydia.EMPLEADO D ON A.EMPLEADO = D.EMPLEADO INNER JOIN EXACTUS.lalydia.DEPARTAMENTO E ON D.DEPARTAMENTO = E.DEPARTAMENTO '+
    ' WHERE A.NUMERO_NOMINA =  @NOM AND A.TOTAL > 0 AND A.CONCEPTO IN ( SELECT CONCEPTO FROM lalydia.CONCEPTO )' +
    ' ORDER BY D.DEPARTAMENTO ASC,A.NUMERO_NOMINA DESC,D.NOMBRE ASC ' ,


   { type: sequelize.QueryTypes.SELECT}
 );
};


bodega.consumoLote= function (fechaInicio,fechaFinal,centroCosto) {
 return sequelize.query(


   'select * from lalydia.TRANSACCION_INV  WHERE FECHA  >=  '+ "'"+fechaInicio+"'" +
   ' AND FECHA < '+ "'"+fechaFinal+"'" +
   ' AND CENTRO_COSTO like '+"'4%'"+
   ' AND CENTRO_COSTO like '+ "'%"+centroCosto+"%'",

   { type: sequelize.QueryTypes.SELECT}
 );
};



bodega.consumoGeneral= function (fechaInicio,fechaFinal) {
 return sequelize.query(

   'select * from lalydia.TRANSACCION_INV  WHERE FECHA  >=  '+ "'"+fechaInicio+"'" +
   'AND FECHA < '+ "'"+fechaFinal+"'" +
   'AND CENTRO_COSTO like '+"'4%'" ,

   { type: sequelize.QueryTypes.SELECT}
 );
};
/*se puede reutilizar el query para la consulta de gastos semanales*/
bodega.generalNomina= function (fechaInicio,fechaFinal) {
 return sequelize.query(

    'SELECT  EMP.TOTAL,C.CONCEPTO,CP.CENTRO_COSTO,NOMI.FECHA_PAGO FROM  lalydia.CONCEPTO C' +
      ' INNER JOIN lalydia.CENTRO_CONCEPTO CP ON  C.CONCEPTO = CP.CONCEPTO ' +
      ' INNER JOIN lalydia.EMPLEADO_CONC_NOMI EMP ON EMP.CONCEPTO=C.CONCEPTO ' +
      ' INNER JOIN lalydia.NOMINA_HISTORICO NOMI ON NOMI.NUMERO_NOMINA=EMP.NUMERO_NOMINA ' +
      ' WHERE CP.CENTRO_COSTO like '+"'4%'" +'AND C.CONCEPTO LIKE '+"'4%'" +'AND EMP.TOTAL>0'+
      ' AND NOMI.FECHA_PAGO >= '+ "'"+fechaInicio+"'" + 'AND NOMI.FECHA_PAGO < '+ "'"+fechaFinal+"'" ,

   { type: sequelize.QueryTypes.SELECT}
 );
};


bodega.generalNominaLote= function (fechaInicio,fechaFinal,centroCosto) {
 return sequelize.query(



   ' SELECT C.CONCEPTO,SUM(EMP.TOTAL) AS TOTAL_NOMINA ,EMP.CENTRO_COSTO,C.UNIDADES,C.DESCRIPCION,NOMI.FECHA_PAGO,sum(EMP.CANTIDAD) as CANTIDAD' +
   ' from lalydia.EMPLEADO_CONC_NOMI EMP '  +
   ' INNER JOIN lalydia.CONCEPTO C ON C.CONCEPTO=EMP.CONCEPTO ' +
   ' INNER JOIN lalydia.NOMINA_HISTORICO NOMI ON NOMI.NUMERO_NOMINA=EMP.NUMERO_NOMINA ' +
   ' WHERE  NOMI.FECHA_PAGO >= '+ "'"+fechaInicio+"'"  +' AND EMP.CONCEPTO LIKE '+"'4%'" +
   ' AND NOMI.FECHA_PAGO <= '+ "'"+fechaFinal+"'" +
   ' AND EMP.CENTRO_COSTO='+ "'"+centroCosto+"'" +
   ' GROUP BY C.CONCEPTO,EMP.CENTRO_COSTO,C.DESCRIPCION,NOMI.FECHA_PAGO,C.UNIDADES',




         /*
     'SELECT CP.CENTRO_COSTO,CP.CONCEPTO, SUM(EMP.TOTAL) AS TOTAL,tr.BODEGA,SUM(tr.CANTIDAD) AS CANTIDAD,SUM(tr.COSTO_TOT_FISC_LOC) AS COLONES,SUM(tr.COSTO_TOT_FISC_DOL) AS DOLARES FROM  LA_LYDIA.CONCEPTO C'+
       ' INNER JOIN LA_LYDIA.CENTRO_CONCEPTO CP ON  C.CONCEPTO = CP.CONCEPTO ' +
       ' INNER JOIN LA_LYDIA.EMPLEADO_CONC_NOMI EMP ON EMP.CONCEPTO=C.CONCEPTO ' +
       ' INNER JOIN LA_LYDIA.NOMINA_HISTORICO NOMI ON NOMI.NUMERO_NOMINA=EMP.NUMERO_NOMINA '+
       ' INNER JOIN LA_LYDIA.TRANSACCION_INV  tr ON tr.CENTRO_COSTO=CP.CENTRO_COSTO ' +
       ' WHERE CP.CENTRO_COSTO like '+"'4%'" +'AND C.CONCEPTO LIKE '+"'4%'" +'AND EMP.TOTAL>0'+
       ' AND NOMI.FECHA_PAGO >= '+ "'"+fechaInicio+"'"  +'AND NOMI.FECHA_PAGO < '+ "'"+fechaFinal+"'" +
       ' AND CP.CENTRO_COSTO = '+ "'"+centroCosto+"'" +  'AND CP.CONCEPTO LIKE '+ "'"+concepto+"'" +
       ' GROUP BY CP.CONCEPTO,CP.CENTRO_COSTO,tr.BODEGA ' +
       ' ORDER BY  CP.CONCEPTO ' ,
     */
   { type: sequelize.QueryTypes.SELECT}
 );
};


bodega.materialesLote= function (fechaInicio,fechaFinal,centroCosto) {
 return sequelize.query(


   ' SELECT tr.CENTRO_COSTO,art.UNIDAD_EMPAQUE,art.DESCRIPCION,art.ARTICULO,tr.BODEGA,SUM(tr.CANTIDAD) AS CANTIDAD, ' +
      ' SUM(tr.COSTO_TOT_FISC_LOC)AS COLONES ,SUM(tr.COSTO_TOT_FISC_DOL) AS DOLARES  from lalydia.TRANSACCION_INV tr ' +
      ' inner join lalydia.ARTICULO art  on tr.ARTICULO = art.ARTICULO ' +
      ' WHERE tr.FECHA_HORA_TRANSAC  >= '+ "'"+fechaInicio+"'" +
      ' AND tr.FECHA_HORA_TRANSAC < '+ "'"+fechaFinal+"'" +
      ' AND tr.CENTRO_COSTO = '+ "'"+centroCosto+"'" +
      ' GROUP BY art.ARTICULO,art.UNIDAD_EMPAQUE,art.DESCRIPCION,tr.BODEGA,tr.CENTRO_COSTO ' ,



   { type: sequelize.QueryTypes.SELECT}
 );
};



bodega.cuentaCombustible= function (fechaInicio,fechaFinal,centroCosto) {
 return sequelize.query(


   'select * from lalydia.CUENTA_CONTABLE where DESCRIPCION like '+"'%Combustible%'"  ,



   { type: sequelize.QueryTypes.SELECT}
 );
};


return bodega;
};

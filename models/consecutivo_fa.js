'use strict';
module.exports = (sequelize, DataTypes) => {
var CONSECUTIVO_FA = sequelize.define('CONSECUTIVO_FA', {
  CODIGO_CONSECUTIVO: DataTypes.STRING,
  VALOR_CONSECUTIVO: DataTypes.STRING
}, {
 timestamps: false,
 hasTrigger: true,
 freezeTableName: true,
 tableName: 'CONSECUTIVO_FA'
});

//Actualizar el consecutivo Pedido
CONSECUTIVO_FA.updateConsecutivo = (pedido) => {
  return sequelize.query(
    `UPDATE lalydia.CONSECUTIVO_FA SET VALOR_CONSECUTIVO = '${pedido}' where CODIGO_CONSECUTIVO LIKE  'PREFACT'`,
    { type: sequelize.QueryTypes.SELECT }
  );
};

//Actualizar el consecutivo Consumo
CONSECUTIVO_FA.updateConsumo = (movimiento) => {
  return sequelize.query(
    `UPDATE lalydia.CONSECUTIVO_CI  SET SIGUIENTE_CONSEC = '${movimiento}'  where DESCRIPCION like 'CONSUMO'`,
    { type: sequelize.QueryTypes.SELECT }
  );
};

//Obtiene el consecutivo Compra
CONSECUTIVO_FA.consecutivoCompra = () => {
  return sequelize.query(
    `select SIGUIENTE_CONSEC from lalydia.CONSECUTIVO_CI cc where DESCRIPCION like 'COMPRA'`,
    { type: sequelize.QueryTypes.SELECT }
  );
};

//Actualizar el consecutivo Compra
CONSECUTIVO_FA.updateConsumoCompra = (compra) => {
  return sequelize.query(
    `UPDATE lalydia.CONSECUTIVO_CI  SET SIGUIENTE_CONSEC = '${compra}'  where DESCRIPCION like 'COMPRA'`,
    { type: sequelize.QueryTypes.SELECT }
  );
};

return CONSECUTIVO_FA;
};

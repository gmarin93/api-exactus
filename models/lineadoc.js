'use strict';
module.exports = (sequelize, DataTypes) => {
var DOCUMENTO_INV_LINEA = sequelize.define('LINEA_DOC_INV', {
  RowPointer: { type: DataTypes.STRING, autoIncrement: true, primaryKey: true },
  PAQUETE_INVENTARIO: DataTypes.STRING,
  DOCUMENTO_INV: DataTypes.STRING,
  LINEA_DOC_INV: DataTypes.INTEGER,
  ARTICULO: DataTypes.STRING,
  BODEGA: DataTypes.STRING,
  LOCALIZACION: DataTypes.STRING,
  LOTE: DataTypes.STRING,
  TIPO: DataTypes.STRING,
  SUBTIPO: DataTypes.STRING,
  SUBSUBTIPO: DataTypes.STRING,
  CANTIDAD: DataTypes.STRING,
  COSTO_TOTAL_LOCAL: DataTypes.STRING,
  COSTO_TOTAL_DOLAR: DataTypes.STRING,
  PRECIO_TOTAL_LOCAL: DataTypes.STRING,
  PRECIO_TOTAL_DOLAR: DataTypes.STRING,
  BODEGA_DESTINO: DataTypes.STRING,
  LOCALIZACION_DEST: DataTypes.STRING,
  CENTRO_COSTO: DataTypes.STRING,
  AJUSTE_CONFIG: DataTypes.STRING,
  SECUENCIA:DataTypes.STRING,
  NIT:DataTypes.STRING,
  SERIE_CADENA:DataTypes.STRING,
  UNIDAD_DISTRIBUCIO:DataTypes.STRING,
  CUENTA_CONTABLE:DataTypes.STRING,
  COSTO_TOTAL_LOCAL_COMP:DataTypes.STRING,
  COSTO_TOTAL_DOLAR_COMP:DataTypes.STRING,
  CAI:DataTypes.STRING,
  externoid:DataTypes.STRING,
  NoteExistsFlag:DataTypes.STRING,
  RecordDate:DataTypes.TIME,
  CreatedBy: DataTypes.STRING,
  UpdatedBy: DataTypes.STRING,
  CreateDate: DataTypes.TIME
}, {
 timestamps: false,
 hasTrigger: true,
 freezeTableName: true,
 tableName: 'LINEA_DOC_INV'
});

// DOCUMENTO_INV_LINEA.associate = function(models) {
    
//     DOCUMENTO_INV_LINEA.hasMany(models.prd_bloquemapaestilo, {   
//     foreignKey:'bloqueid'
//    });

//    DOCUMENTO_INV_LINEA.belongsTo(models.prd_lotes,{
//     foreignKey:'loteid'
//   });

// };


return DOCUMENTO_INV_LINEA;
};

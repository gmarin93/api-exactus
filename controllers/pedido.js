const { getFecha, getHora } = require("../helper/helperFecha");
const models = require("../models");
const { v4: uuidv4 } = require('uuid');

// Obtiene la lista de precios articulos por cliente
exports.listaPreciosPedido = async (req, res) => {
  const data = req.body;

  try {
    let lista_precios = [];
    let versionPrecio;
    for (const cliente of data) {
      
      const infoCliente = await models.PEDIDO.infoCliente(cliente.codigo);

      versionPrecio = await models.PEDIDO.versionPrecio(infoCliente[0].NIVEL_PRECIO);

      const precios = await models.PEDIDO.obtieneArticulosPorCliente(
        infoCliente[0].NIVEL_PRECIO,
        versionPrecio[0].VERSION
      );

      if (precios.length > 0) {
        precios.map((p) => {
          p.CLIENTE = cliente.codigo;
          lista_precios.push(p);
        });
      }
    }
    res.json(lista_precios);
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

// Obtiene la lista de precios articulos por cliente
exports.listaPreciosArticulos = async (req, res) => {
  const data = req.body;
  const {finicio, ffin} = req.params;

  try {
    let lista_precios = [];
    let versionPrecio;
    for (const cliente of data) {
      const infoCliente = await models.PEDIDO.infoCliente(cliente.codigo);
       
      versionPrecio = await models.PEDIDO.versionPrecioPorSemana(cliente.codigo, finicio, ffin);   

      if(versionPrecio.length === 0){
       versionPrecio = await models.PEDIDO.versionPrecio(infoCliente[0].NIVEL_PRECIO);
      }
      const precios = await models.PEDIDO.obtieneArticulosPorCliente(
        infoCliente[0].NIVEL_PRECIO,
        versionPrecio[0].VERSION
      );

      if (precios.length > 0) {
        precios.map((p) => {
          p.CLIENTE = cliente.codigo;
          lista_precios.push(p);
        });
      }
    }
    res.json(lista_precios);
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

// Obtiene las direcciones por cliente
exports.direccionesCliente = async (req, res) => {
  const {cliente} = req.params;

  try {
    const direcciones = await models.PEDIDO.getDireccionEmbarque(cliente);
    res.json(direcciones);
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

// Obtiene los clientes
exports.getClientes = async (req, res) => {
  try {
    const clientes = await models.PEDIDO.getClientes();
    res.json(clientes);
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

// Obtiene las facturas de un arreglo
exports.getFacturas = async (req, res) => {
  const {arreglo} = req.body;

  try {
    const consulta = await models.PEDIDO.getFacturas(arreglo);
    res.json(consulta);
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

// Obtiene los datos del Pedido Cliente
exports.getPedidoCliente = async (req, res) => {
  const { pedido } = req.params;
  try {
    const consulta = await  models.PEDIDO.findOne({
      attributes: ['DIREC_EMBARQUE','RUBRO1','RUBRO2','RUBRO3','RUBRO4','RUBRO5','OBSERVACIONES','DESC_DIREC_EMBARQUE'],
      where: {PEDIDO: pedido}
    });
    res.json(consulta);
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

// Creacion de prefacturacion
exports.crearOrdenPedido = async (req, res) => {
  let lineas = req.body;
  const { cliente,factura } = req.params;


  try {
    let pedidoFormatear ;
    // Obtiene el ultimo id de Pedido
    if(factura == 'CREAR'){
      const pedido = await models.PEDIDO.ultimoIdPedido();
      pedidoFormatear = pedido[0].PEDIDO.split("-", 2);
      pedidoFormatear = `${pedidoFormatear[0]}-${parseInt(pedidoFormatear[1]) + 1}`;

       // Actualiza el consecutivo del PEDIDO
     await  models.CONSECUTIVO_FA.updateConsecutivo(pedidoFormatear);
    }else{
      pedidoFormatear = factura;
      // Obtiene el estado del pedido
      const estadoPedido = await models.PEDIDO.getEstadoPedido(factura);
      if(estadoPedido[0].ESTADO !== 'F'){
        await models.PEDIDO_LINEA.destroy({where: { PEDIDO:factura }});
        await models.PEDIDO.destroy({where: { PEDIDO:factura }});   
      }else{
        return res.status(400).json("Este pedido ya fue facturado")
      }        
    }   

     // Obtiene informacion del cliente, parametro es el codigo del cliente
    const infoCliente = await models.PEDIDO.infoCliente(cliente);

    const versionPrecio = await models.PEDIDO.versionPrecio(infoCliente[0].NIVEL_PRECIO);
    infoCliente[0].VERSION_NP = versionPrecio[0].VERSION;

    // EXONERACION DEL CLIENTE
    const exoneracion_cliente  = await models.PEDIDO.getExoneracion(cliente);

    // Verifica si el cliente tiene exoneracion
    let porcentaje_impuesto = 0; 
    if(infoCliente[0].LOCAL == 'L'){
     
      if(exoneracion_cliente.length > 0){
        porcentaje_impuesto = 0;
      } else{
        porcentaje_impuesto = 1;
      }
    }   

    // Obtener total mercaderia, a facturar, total unidades, base impuestos
    let total_unidades = 0;
    let total_mercaderia = 0; 
    let impuestos = 0;
    let exoneracion = 0;

    for (const linea of lineas.ARTICULOS) {  
      total_unidades += linea.CANTIDAD_PEDIDA;
      total_mercaderia += linea.PRECIO_UNITARIO * linea.CANTIDAD_PEDIDA;
      let precioUnitario  = parseFloat(linea.PRECIO_UNITARIO);
      let cantidaPedida  = parseFloat(linea.CANTIDAD_PEDIDA);
      if(exoneracion_cliente.length > 0){
        let exoneracion_art = await  models.PEDIDO.getExoneracionArticulo(exoneracion_cliente[0].CODIGO, linea.ARTICULO);
        if(exoneracion_art.length > 0 && infoCliente[0].LOCAL == 'L'){
          impuestos +=  (cantidaPedida * precioUnitario) * 0.01;
          exoneracion += (cantidaPedida * precioUnitario) * 0.01;
          linea.PORC_EXONERACION	=	0.01;
          linea.MONTO_EXONERACION = (cantidaPedida * precioUnitario) * 0.01;
        } else {
          linea.PORC_EXONERACION	=	0,
          linea.MONTO_EXONERACION	= 0;
          impuestos +=  (cantidaPedida * precioUnitario) * 0.01;
        }
      }
      
    }
    // Se calcula el total de impuestos
    const total_impuestos = impuestos - exoneracion;
    const observacionPedido = lineas.OBSERVACIONES.replace("\n",String.fromCharCode(13));
    let objetoPedido = {
          RowPointer: uuidv4(),
          PEDIDO: pedidoFormatear,
          TIPO_DOC_CXC:	'FAC',
          SUBTIPO_DOC_CXC:	0,
          BODEGA:	'06',
          CLIENTE: cliente,
          RUTA:	infoCliente[0].RUTA,
          ZONA:	infoCliente[0].ZONA,
          PAIS:	infoCliente[0].PAIS,
          VENDEDOR:	infoCliente[0].VENDEDOR,
          COBRADOR:	infoCliente[0].COBRADOR,
          CONDICION_PAGO:	infoCliente[0].CONDICION_PAGO,
          ESTADO:	'N',
          FECHA_PEDIDO: `${getFecha()} ${getHora()}`,
          FECHA_PROMETIDA	: `${getFecha()} ${getHora()}`,
          FECHA_PROX_EMBARQU	: `${getFecha()} ${getHora()}`,
          FECHA_ULT_EMBARQUE	:`${getFecha()} ${getHora()}`,
          FECHA_ULT_CANCELAC	: `${getFecha()} ${getHora()}`,
          FECHA_ORDEN	: `${getFecha()} ${getHora()}`,
          EMBARCAR_A:	infoCliente[0].NOMBRE,

          DIREC_EMBARQUE:	lineas.DIREC_EMBARQUE,
          RUBRO1:	lineas.RUBRO1,
          RUBRO2:	lineas.RUBRO2,
          RUBRO3:	lineas.RUBRO3,
          RUBRO4:	lineas.RUBRO4,
          RUBRO5:	lineas.RUBRO5,
          OBSERVACIONES :observacionPedido,
          TOTAL_MERCADERIA:	total_mercaderia,
          MONTO_ANTICIPO:	0,
          MONTO_FLETE:	0,
          MONTO_SEGURO:	0,
          MONTO_DOCUMENTACIO:	0,
          TIPO_DESCUENTO1:'P',
          TIPO_DESCUENTO2	:'P',
          MONTO_DESCUENTO1:	infoCliente[0].DESCUENTO,
          MONTO_DESCUENTO2:	infoCliente[0].DESCUENTO,
          PORC_DESCUENTO1	:	infoCliente[0].DESCUENTO,
          PORC_DESCUENTO2	:	infoCliente[0].DESCUENTO,
          TOTAL_IMPUESTO1: total_impuestos,//Number.parseFloat( Number.parseFloat(impuestos) -  Number.parseFloat(exoneracion)).toFixed(2),
          TOTAL_IMPUESTO2	:0,

          TOTAL_A_FACTURAR	:total_mercaderia,
          PORC_COMI_VENDEDOR:	0,
          PORC_COMI_COBRADOR:	0,
          TOTAL_CANCELADO	:	0,
          TOTAL_UNIDADES:	total_unidades,
          IMPRESO:'N',
          USUARIO:'CONTAB1',
          FECHA_HORA: `${getFecha()} ${getHora()}`,
          DESCUENTO_VOLUMEN:	0,
          TIPO_PEDIDO:	'N',
          MONEDA_PEDIDO	: infoCliente[0].MONEDA,
          CLASE_PEDIDO:	'N',
          NIVEL_PRECIO: infoCliente[0].NIVEL_PRECIO,
          MONEDA: infoCliente[0].MONEDA,
          VERSION_NP:	infoCliente[0].VERSION_NP,
          AUTORIZADO:	'N',
          DOC_A_GENERAR	:	'F',
          CLIENTE_ORIGEN:	cliente,
          CLIENTE_CORPORAC:cliente,
          CLIENTE_DIRECCION:cliente,
          BACKORDER	:	'N',
          DESCUENTO_CASCADA	:	'N',
          DIRECCION_FACTURA	:	infoCliente[0].DIRECCION,
          PORC_INTCTE:	0,
          FIJAR_TIPO_CAMBIO:	'N',
          ORIGEN_PEDIDO	:	'F',
          DIVISION_GEOGRAFICA1	:	infoCliente[0].DIVISION_GEOGRAFICA1,
          DIVISION_GEOGRAFICA2	:	infoCliente[0].DIVISION_GEOGRAFICA2,
          BASE_IMPUESTO1:	total_mercaderia,
          BASE_IMPUESTO2:	total_mercaderia,
          NOMBRE_CLIENTE:	infoCliente[0].NOMBRE,
          FECHA_PROYECTADA: `${getFecha()} ${getHora()}`,
          TIPO_DOCUMENTO:	'P',
          TASA_IMPOSITIVA_PORC:	0,
          TASA_CREE1_PORC:	0,
          TASA_CREE2_PORC:	0,
          TASA_GAN_OCASIONAL_PORC:	0,
          CONTRATO_REVENTA:	'N',
          ACTIVIDAD_COMERCIAL:'011340',
          MONTO_OTRO_CARGO: 0,
          NoteExistsFlag: 0,
          RecordDate: `${getFecha()} ${getHora()}`,
          CreatedBy:	'FA/CONTAB1',
          UpdatedBy:	'FA/CONTAB1',
          CreateDate	: `${getFecha()} ${getHora()}`,
          ES_FACTURA_REEMPLAZO: 'N'
    }
    // Se crea el PEIDO aqui
    await  models.PEDIDO.create(objetoPedido);    
    let numero_linea = 0;
    const cuenta_contable = infoCliente[0].LOCAL == 'L' ?  '4-01-01-01-001' : '4-01-01-02-001';
    const porc_impuesto1  = porcentaje_impuesto == 0 ? 0 : 1;
    const tipo_tarifa = infoCliente[0].LOCAL == 'L' ?  '02' : '01';

    for (const linea of lineas.ARTICULOS) {    
      //CREACION DEL OBJETO PEDIDO_LINEA   {linea.PRECIO_UNITARIO, linea.CANTIDAD_PEDIDA , linea.ARTICULO, linea.ES_CANASTA_BASICA}
      let porc_imp1_base  = 0;
      if(tipo_tarifa == '02'){
        porc_imp1_base = 1;
      }

      let objetoLineaPedido = {
      PEDIDO	:	pedidoFormatear	,
      PEDIDO_LINEA:	numero_linea + 1 ,
      ARTICULO	:	linea.ARTICULO,
      BODEGA	:	'06',
      ESTADO	:	'N'	,
      FECHA_ENTREGA	:	`${getFecha()} ${getHora()}`,
      LINEA_USUARIO	:	numero_linea,
      PRECIO_UNITARIO	:linea.PRECIO_UNITARIO	,
      CANTIDAD_PEDIDA	:	linea.CANTIDAD_PEDIDA	,
      CANTIDAD_A_FACTURA	:	linea.CANTIDAD_PEDIDA	,
      CANTIDAD_FACTURADA	:	0	,
      CANTIDAD_RESERVADA	:	0	,
      CANTIDAD_BONIFICAD	:	0	,
      CANTIDAD_CANCELADA	:	0	,
      TIPO_DESCUENTO	:	'P'	,
      LOCALIZACION: '001',
      MONTO_DESCUENTO	:	0	,
      PORC_DESCUENTO	:	0	,
      FECHA_PROMETIDA	:		`${getFecha()} ${getHora()}`,
      CENTRO_COSTO	:	'02-99-00-00',
      CUENTA_CONTABLE	:	cuenta_contable,
      TIPO_DESC	:	0	,
      TIPO_IMPUESTO1	:	'01',
      TIPO_TARIFA1	:	tipo_tarifa,    
      PORC_EXONERACION	:	linea.PORC_EXONERACION	,
      MONTO_EXONERACION	:	linea.MONTO_EXONERACION	,
      PORC_IMPUESTO2	:	0	,
      ES_OTRO_CARGO	:	'N'	,
      ES_CANASTA_BASICA	:	linea.ES_CANASTA_BASICA	,
      NoteExistsFlag	:	0	,
      RecordDate	:	`${getFecha()} ${getHora()}`,
      CreatedBy	:	'FA/CONTAB1'	,
      UpdatedBy	:	'FA/CONTAB1'	,
      CreateDate	:		`${getFecha()} ${getHora()}`,    
      // tipo impuesto/ tipo tarifa
      PORC_IMP1_BASE:porc_imp1_base, 

      PORC_IMPUESTO1:	porc_impuesto1 ,
      PORC_IMP2_BASE	:	0	,
     }
     await  models.PEDIDO_LINEA.create(objetoLineaPedido)
     numero_linea+=1;       
    }
    
     res.json({pedido: pedidoFormatear, version:infoCliente[0].VERSION_NP});
  } catch (err) {
    console.log(err);
    res.status(500).send({    
      message: err || errorDb,
    });
  }
};

const { formatearConsecutivo } = require("../helper/formatearConsecutivo");
const { getFecha, getHora } = require("../helper/helperFecha");
const models = require("../models");

/**
 * Obtiene los insumos de la bodega 3 para el software de maderas
 * @param {*} req
 * @param {*} res
 */
exports.obtenerInsumos = async (req, res) => {
  try {
    let insumos = await models.ARTICULO.obtenerInsumosGanadero();
    res.json(insumos);
  } catch (err) {
    console.log(err);
    res.status(500).send({
      message: err || errorDb,
    });
  }
};

/**
 * Obtiene el informacion de un articulo por codigo
 * @param {*} req
 * @param {*} res
 */

// Insumo por codigo
exports.obtenerInsumoPorCodigo = async (req, res) => {
  const { codigo } = req.params;
  try {
    let insumo = await models.ARTICULO.obtenerInsumoPorCodigo(codigo);
    res.json(insumo);
  } catch (err) {
    console.log(err);
    res.status(500).send({
      message: err || errorDb,
    });
  }
};

/**
 * Obtiene los centro costo ganadero
 * @param {*} req
 * @param {*} res
 */
exports.obtenerCentrosCostoGanadero = async (req, res) => {
  try {
    let centro_costo = await models.CENTRO_COSTO.obtenerCentrosCostoGanadero();
    res.json(centro_costo);
  } catch (err) {
    console.log(err);
    res.status(500).send({
      message: err || errorDb,
    });
  }
};

/**
 * Obtiene los centro costo ganadero
 * @param {*} req
 * @param {*} res
 */
exports.obtenerCuentaContableGanadero = async (req, res) => {
  try {
    let cuenta_contables =
      await models.CUENTA_CONTABLE.obtenerCuentaContableGanadero();
    res.json(cuenta_contables);
  } catch (err) {
    console.log(err);
    res.status(500).send({
      message: err || errorDb,
    });
  }
};

/**
 * Obtiene las bodegas de un insumo
 * @param {*} req
 * @param {*} res
 */
exports.obtenerBodegasInsumos = async (req, res) => {
  const { codigo } = req.params;
  try {
    let bodegas = await models.ARTICULO.obtenerBodegasInsumos(codigo);
    res.json(bodegas);
  } catch (err) {
    console.log(err);
    res.status(500).send({
      message: err || errorDb,
    });
  }
};

/**
 * Crear la Requisicion de raciones
 * @param {*} req
 * @param {*} res
 */
exports.crearRequisicion = async (req, res) => {
  const { REFERENCIA, INSUMOS, FECHA } = req.body;
  try {
    // Obtener el consecutivo
    let consecutivo = await models.DOCUMENTO_INV.últimoID();
    // Formatea el consecutivo para obtener el siguiente
    consecutivo = formatearConsecutivo(consecutivo[0].DOCUMENTO_INV);
    // Objeto del papa de requisicion
    let obtDocumentoInv = {
      REFERENCIA: REFERENCIA,
      SELECCIONADO: "N",
      MENSAJE_SISTEMA: "",
      CONSECUTIVO: "CONSUMO",
      DOCUMENTO_INV: consecutivo,
      FECHA_HOR_CREACION: `${FECHA} ${getHora()}`,
      FECHA_DOCUMENTO: `${FECHA} ${getHora()}`,
      RecordDate: `${FECHA} ${getHora()}`,
      CreateDate: `${FECHA} ${getHora()}`,

      // ******************** CAMBIAR ESTO
      USUARIO: "ASISTLECHE",
      PAQUETE_INVENTARIO: "L3",
      CreatedBy: "CI/ASISTLECHE",
      UpdatedBy: "CI/ASISTLECHE",
    };
    // Creacion del papá
    await models.DOCUMENTO_INV.create(obtDocumentoInv);
    let contador = 1;
    // Recorre todas la lineas de insumos de la racion
    for (const insumo of INSUMOS) {
      let obtLineaDocInv = {
        DOCUMENTO_INV: consecutivo,
        LINEA_DOC_INV: contador,
        ARTICULO: insumo.codigo,
        TIPO: "C",
        SUBTIPO: "D",
        SUBSUBTIPO: "N",
        CANTIDAD: insumo.cantidad,
        LOCALIZACION: "001",
        COSTO_TOTAL_LOCAL: 0,
        COSTO_TOTAL_DOLAR: 0,
        PRECIO_TOTAL_LOCAL: 0,
        PRECIO_TOTAL_DOLAR: 0,
        COSTO_TOTAL_LOCAL_COMP: 0,
        COSTO_TOTAL_DOLAR_COMP: 0,
        CENTRO_COSTO: insumo.centro_costo,

        // ******************** CAMBIAR ESTO
        BODEGA: insumo.bodega,
        PAQUETE_INVENTARIO: "L3",
        CUENTA_CONTABLE: insumo.cuenta_contable,
        CreatedBy: "CI/ASISTLECHE",
        UpdatedBy: "CI/ASISTLECHE",

        AJUSTE_CONFIG: "~CC~",
        NoteExistsFlag: 0,
        RecordDate: `${FECHA} ${getHora()}`,
        CreateDate: `${FECHA} ${getHora()}`,
      };

      let doc_inv_fase = {
        PAQUETE_INVENTARIO: "L3",
        DOCUMENTO_INV: consecutivo,
        LINEA_DOC_INV: contador,
        FASE: insumo.fase,
        ORDEN_CAMBIO: 1,
        NoteExistsFlag: 0,
        RecordDate: `${FECHA} ${getHora()}`,
        CreatedBy: "CI/ASISTLECHE",
        UpdatedBy: "CI/ASISTLECHE",
        CreateDate: `${FECHA} ${getHora()}`,
      };
      
      // Creacion de las línea
      await models.LINEA_DOC_INV.create(obtLineaDocInv);
      
      // Crear DOC_INV_FASE_PY
      await models.DOC_INV_FASE_PY.create(doc_inv_fase)

      contador += 1;
    }

    // Actualiza el consecutivo de Consumo
    await models.CONSECUTIVO_FA.updateConsumo(consecutivo);

    res.json(consecutivo);
  } catch (err) {
    console.log(err);
    res.status(500).send({
      message: err || errorDb,
    });
  }
};

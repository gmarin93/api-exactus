const { getFecha, getHora } = require("../helper/helperFecha");
const models = require("../models");
const { formatearConsecutivo } = require("../helper/formatearConsecutivo");


// Creacion de prefacturacion
exports.cargarInventario = async (req, res) => {
 let lineas = req.body;

  try {
    
    let consecutivo = await models.CONSECUTIVO_FA.consecutivoCompra();
    consecutivo = formatearConsecutivo(consecutivo[0].SIGUIENTE_CONSEC);
    let documento_inv = {
      PAQUETE_INVENTARIO	:	'W2',
      DOCUMENTO_INV	:	consecutivo,
      REFERENCIA	:	'Carga de inventario a Exactus desde eAgro a la planta empacadora',
      FECHA_HOR_CREACION	:	`${getFecha()} ${getHora()}`,
      FECHA_DOCUMENTO	:	`${getFecha()} ${getHora()}`,
      SELECCIONADO	:	'N',
      USUARIO	:	'SA',
      MENSAJE_SISTEMA:'',
      CONSECUTIVO	:	'COMPRA',
      NoteExistsFlag	:	'0',
      RecordDate	:	`${getFecha()} ${getHora()}`,
      CreatedBy	:	'CI/SA',
      UpdatedBy	:	'CI/SA',
      CreateDate	:	`${getFecha()} ${getHora()}`
    }
    // Creacion del papá
    await models.DOCUMENTO_INV.create(documento_inv);

    let contador = 1;
    for( let linea of lineas ){
      const articulo = await models.ARTICULO.obtenerCostoArticulo(linea.articulo);
      if(articulo.length > 0){
        let linea_doc_inv = {
          PAQUETE_INVENTARIO	:	'W2'	,
          DOCUMENTO_INV	:	consecutivo	,
          LINEA_DOC_INV	:	contador 	,
          ARTICULO	:linea.articulo,
          BODEGA	:	'06'	,
          LOCALIZACION	:	'001'	,
          TIPO	:	'O'	,
          SUBTIPO	:	'D'	,
          SUBSUBTIPO	:	'L'	,
          CANTIDAD	:	linea.totalcajas	,
          COSTO_TOTAL_LOCAL	:	articulo[0].COSTO_PROM_LOC,
          COSTO_TOTAL_DOLAR	:		articulo[0].COSTO_PROM_DOL,	
          PRECIO_TOTAL_LOCAL	:	0	,
          PRECIO_TOTAL_DOLAR	:	0	,
          AJUSTE_CONFIG	:	'~OO~'	,
          COSTO_TOTAL_LOCAL_COMP	:	articulo[0].COSTO_PROM_LOC,
          COSTO_TOTAL_DOLAR_COMP	:	articulo[0].COSTO_PROM_DOL,	
          NoteExistsFlag	:	'0'	,
          RecordDate	:	`${getFecha()} ${getHora()}`,
          CreatedBy	:	'CI/SA'	,
          UpdatedBy	:	'CI/SA'	,
          CreateDate	:	`${getFecha()} ${getHora()}`
        }
        // Creacion del hijo
        await models.LINEA_DOC_INV.create(linea_doc_inv)
      
        contador+=1;
      }else{
        console.log("VERIFICAR, NO ENCONTRO ARTICULO (COSTOS)")
      }
     
    }

     // Actualiza el consecutivo de Compra
     await  models.CONSECUTIVO_FA.updateConsumoCompra(consecutivo)    
     
    res.json({consecutivo})
  } catch (err) {
    console.log( err || errorDb);
    res.status(500).send({    
      message: err || errorDb,
    });
  }
};

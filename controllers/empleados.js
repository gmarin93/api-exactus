const { getFecha, getHora } = require("../helper/helperFecha");
const models = require("../models");
const { v4: uuidv4 } = require('uuid');

// Empleados departamento de operadores
exports.obtenerEmpleadosMaquinaria = async (req, res) => {
  try { 
    // const empleados = await models.EMPLEADO.obtenerEmpleadosMaquinaria();   
    const empleados =  await models.EMPLEADO.findAll({attributes: ["NOMBRE" ,"EMPLEADO"]
    ,where: { ACTIVO:'S', DEPARTAMENTO:'OM' },raw: true});
    res.json(empleados)
  } catch (err) {
    console.log(err);
    res.status(500).send({    
      message: err || errorDb,
    });
  }
};

// Obtener empleado por codigo
exports.obtenerEmpleadoPorCodigo = async (req, res) => {
  const { codigo } = req.params;
  try { 
    let empleado;
    empleado = await models.EMPLEADO.findOne({attributes: ["PRIMER_APELLIDO" ,"SEGUNDO_APELLIDO" ,"NOMBRE_PILA","EMPLEADO","NOMBRE","ACTIVO","IDENTIFICACION","ASEGURADO","FECHA_INGRESO","SALARIO_REFERENCIA","SEXO"]
    ,where: { EMPLEADO:codigo },raw: true}); 
    if(!empleado){
      empleado = await models.EMPLEADO.infoEmpleado(codigo);
      empleado = empleado[0];
    }
    res.json(empleado)
  } catch (err) {
    console.log(err);
    res.status(500).send({    
      message: err || errorDb,
    });
  }
};

/**
 * Obtiene todos los empleados La Lydia y Pitalia activos
 * @param {*} req 
 * @param {*} res 
 */
exports.obtenerEmpleadosTodos = async (req, res) => {
  try {
    let empleados =  await models.EMPLEADO.obtenerEmpleadosActivos();
    const empleadosPitalia = await models.EMPLEADO.obtenerEmpleadosActivosPitalia();
    empleados = empleados.concat(empleadosPitalia);
    res.json(empleados)
  } catch (err) {
    console.log(err);
    res.status(500).send({    
      message: err || errorDb,
    });
  }
};

/**
 * Obtiene todos los empleados inactivos La Lydia y Pitalia activos
 * @param {*} req 
 * @param {*} res 
 */
exports.obtenerInactivos = async (req, res) => {
  try {
    let empleados =  await models.EMPLEADO.obtenerEmpleadosInactivosLaLydia();
    const empleadosPitalia = await models.EMPLEADO.obtenerEmpleadosInactivosPitalia();
    empleados = empleados.concat(empleadosPitalia);
    res.json(empleados)
  } catch (err) {
    console.log(err);
    res.status(500).send({    
      message: err || errorDb,
    });
  }
};

/**
 * Obtiene todos los empleados La Lydia y
 * @param {*} req 
 * @param {*} res 
 */
exports.obtenerEmpleadosLaLydia = async (req, res) => {
  try {
    let empleados =  await models.EMPLEADO.obtenerEmpleadosLaLydiaTodos();
    res.json(empleados)
  } catch (err) {
    console.log(err);
    res.status(500).send({    
      message: err || errorDb,
    });
  }
};

/**
 * Obtiene todos los empleados La Lydia y Pitalia
 * @param {*} req 
 * @param {*} res 
 */
exports.obtenerEmpleadosTodosParaIncapacidades = async (req, res) => {
  try {
    let empleados =  await models.EMPLEADO.obtenerEmpleadosLaLydiaTodos();
    const empleadosPitalia = await models.EMPLEADO.obtenerEmpleadosPitaliaTodos();
    empleados = empleados.concat(empleadosPitalia);
    res.json(empleados)
  } catch (err) {
    console.log(err);
    res.status(500).send({    
      message: err || errorDb,
    });
  }
};

/**
 * Obtiene todos los departamentos
 * @param {*} req 
 * @param {*} res 
 */
 exports.obtenerDepartamentos = async (req, res) => {
  try {
    let departamentos =  await models.EMPLEADO.obtenerDepartamentos();
    const departamentosPitalia = await models.EMPLEADO.obtenerDepartamentosPitalia();
    departamentos = departamentos.concat(departamentosPitalia);
    res.json(departamentos)
  } catch (err) {
    console.log(err);
    res.status(500).send({    
      message: err || errorDb,
    });
  }
};

/**
 * Obtiene datos para la planila
 * @param {*} req 
 * @param {*} res 
 */
exports.obtenerDatosParaPlanilla = async (req, res) => {
  try {
    let empleados =  await models.EMPLEADO.obtenerEmpleadosActivos();
    const empleadosPitalia = await models.EMPLEADO.obtenerEmpleadosActivosPitalia();
    empleados = empleados.concat(empleadosPitalia);
    let labores = await models.ARTICULO.obtenerLabores();
    const laboresPitalia = await models.ARTICULO.obtenerLaboresPitalia();
    labores = labores.concat(laboresPitalia);
    let centro_costos = await models.ARTICULO.obtenerCentroCostoPlanillas();   
    const centro_costos_pitalia = await models.ARTICULO.obtenerCentroCostoPlanillasPitalia();
    centro_costos = centro_costos.concat(centro_costos_pitalia)
    const proyectos = await models.ARTICULO.obtenerProyectos();
    const fases = await models.ARTICULO.obtenerFases();
    res.json({empleados, labores, centro_costos, proyectos, fases})
  } catch (err) {
    console.log(err);
    res.status(500).send({    
      message: err || errorDb,
    });
  }
};

/**
 * Crear accion personal, incacapacidad
 * @param {*} req 
 * @param {*} res 
 */
exports.crearIncapacidad = async (req, res) => {
  const data = req.body;
  let  NUMERO_ACCION 
  try {
    let empresa = (data.empresa == 'Pitalia') ? "PITALIA" : "lalydia";

     NUMERO_ACCION  =  await models.EMPLEADO_ACC_PER.obtenerNumeroAccion(`${empresa}.GLOBALES_GN`) 
     NUMERO_ACCION = NUMERO_ACCION[0].ULTIMA_ACCION;
    let objeto = {
      database: `${empresa}.EMPLEADO_ACC_PER`,
      NUMERO_ACCION,
      TIPO_ACCION	: data.ins_ccss,
      FECHA	: `${getFecha()} ${getHora()}`,
      EMPLEADO	: data.codigo,
      TIPO_AUSENCIA	: data.tipo_ausencia,
      FECHA_RIGE	: data.fecha,
      FECHA_VENCE	: data.fecha_reingreso,
      NOTAS: null,
      USUARIO	: "SOCUPACIONAL",
      FECHA_HORA	: `${getFecha()} ${getHora()}`,
      ESTADO_ACCION	: 'N',
      RUBRO3: data.licencia,
      DIAS_ACCION	: data.cantidad_dias,
      SALDO	: data.cantidad_dias,
      RowPointer: uuidv4(),
      BONO_DECRETO	: 0,
      RecordDate	: `${getFecha()} ${getHora()}`,
      CreatedBy	: 'GN/SOCUPACIONAL',
      UpdatedBy	: 'GN/SOCUPACIONAL',
      CreateDate	: `${getFecha()} ${getHora()}`,
    }
    // Se crea la linea
   await  models.EMPLEADO_ACC_PER.crearIncapacidad(objeto.database,objeto.NUMERO_ACCION,objeto.TIPO_ACCION,objeto.FECHA,objeto.EMPLEADO,objeto.TIPO_AUSENCIA,objeto.FECHA_RIGE,objeto.FECHA_VENCE,objeto.NOTAS,objeto.USUARIO,objeto.FECHA_HORA,
   objeto.ESTADO_ACCION,objeto.DIAS_ACCION,objeto.SALDO,objeto.RowPointer,objeto.BONO_DECRETO,objeto.RecordDate,objeto.CreatedBy,objeto.UpdatedBy,objeto.CreateDate,objeto.RUBRO3);    
   
    // actualiza GLOBALES_GN
    const ULTIMA_ACCION = NUMERO_ACCION + 1;
    // 
    if(empresa == 'PITALIA')
      await models.EMPLEADO_ACC_PER.actualizarGlobalGNPitalia(`${empresa}.GLOBALES_GN`,ULTIMA_ACCION,objeto.CreateDate);
    else
      await models.EMPLEADO_ACC_PER.actualizarGlobalGNLaLydia(`${empresa}.GLOBALES_GN`,ULTIMA_ACCION,objeto.CreateDate);
    
    let empleado = await models.EMPLEADO_ACC_PER.obtenerEmpleado(`${empresa}.EMPLEADO`,data.codigo);
    empleado = empleado[0];
    // se crea ACCION_PERSONAL
    await models.EMPLEADO_ACC_PER.crearAccionPersonal(`${empresa}.ACCION_PERSONAL`,NUMERO_ACCION,empleado.EMPLEADO,empleado.DEPARTAMENTO,empleado.PUESTO,empleado.PLAZA,empleado.CENTRO_COSTO,empleado.NOMINA,empleado.ESTADO_EMPLEADO,empleado.SALARIO_REFERENCIA);
    

    res.json({NUMERO_ACCION})
  } catch (err) {
    res.json({NUMERO_ACCION})
  }
};

/**
 * Eliminar ACCION PERSONAL
 * @param {*} req 
 * @param {*} res 
 */
exports.eliminarAccionPersonal = async (req, res) => {
  const {numero_accion, empresa} = req.body;
  try {
    // Verificar la accion personal
    let database = (empresa == 'Pitalia') ? "PITALIA" : "lalydia";
     
    const validar =  await models.EMPLEADO_ACC_PER.validarAprobacionAccionPersonal(`${database}.EMPLEADO_ACC_PER`,numero_accion);
    if(validar.length == 1){
      // Eliminar
      await models.EMPLEADO_ACC_PER.eliminarAccionPersonal(`${database}.ACCION_PERSONAL`,numero_accion)
      res.json(`La acción personal ${numero_accion} fue eliminada exitosamente`)
    }else
     res.status(400).json(`La acción personal ${numero_accion} se encuentra aprobada, debes reversar de la empresa ${empresa}.`)
   
  } catch (err) {
    console.log(err);
    res.status(500).send({    
      message: err || errorDb,
    });
  }
};

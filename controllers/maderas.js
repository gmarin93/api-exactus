const { formatearConsecutivo } = require("../helper/formatearConsecutivo");
const { getFecha, getHora } = require("../helper/helperFecha");
const models = require("../models");

// Centros de Costo
exports.obtenerCentroCosto = async (req, res) => {
  try {    
    let centrocostos = await models.ARTICULO.obtenerCentroCostos();   
    res.json(centrocostos)
  } catch (err) {
    console.log(err);
    res.status(500).send({    
      message: err || errorDb,
    });
  }
};

// Centros de Costo
exports.obtenerLabores = async (req, res) => {
  try {    
    let centrocostos = await models.ARTICULO.obtenerLabores();   
    res.json(centrocostos)
  } catch (err) {
    console.log(err);
    res.status(500).send({    
      message: err || errorDb,
    });
  }
};

// Empleados
exports.obtenerEmpleados = async (req, res) => {
  try {    
    let empleados = await models.ARTICULO.obtenerEmpleados();   
    res.json(empleados)
  } catch (err) {
    console.log(err);
    res.status(500).send({    
      message: err || errorDb,
    });
  }
};

// Empleados por codigo
exports.obtenerEmpleadosPorCodigo = async (req, res) => {
  const {codigo} = req.params;
  try {    
    let empleado = await models.ARTICULO.obtenerEmpleadoPorCodigo(codigo);   
    res.json(empleado)
  } catch (err) {
    console.log(err);
    res.status(500).send({    
      message: err || errorDb,
    });
  }
};

// Insumos
exports.obtenerInsumos = async (req, res) => {
  try {    
    let insumos = await models.ARTICULO.obtenerInsumos();   
    res.json(insumos)
  } catch (err) {
    console.log(err);
    res.status(500).send({    
      message: err || errorDb,
    });
  }
};

// Insumo por codigo
exports.obtenerInsumoPorCodigo = async (req, res) => {
  const {codigo} = req.params;
  try {    
    let insumo = await models.ARTICULO.obtenerInsumoPorCodigo(codigo);   
    res.json(insumo)
  } catch (err) {
    console.log(err);
    res.status(500).send({    
      message: err || errorDb,
    });
  }
};

/**
 * Obtiene las bodegas
 * @param {*} req 
 * @param {*} res 
 */
exports.obtenerBodegas = async (req, res) => {
  try {    
    let bodegas = await models.BODEGA.findAll({attributes: [["BODEGA","value"],["NOMBRE","label"]],where: {'BODEGA': {in: ['01','08','15','16']}},order: [['BODEGA', 'ASC']], raw: true});
    res.json(bodegas)
  } catch (err) {
    console.log(err);
    res.status(500).send({    
      message: err || errorDb,
    });
  }
};

/**
 * Obtiene la cuentas contables
 * @param {*} req 
 * @param {*} res 
 */
exports.obtenerCuentaContables = async (req, res) => {
  try {    
    let cuentascontables = await models.CUENTA_CONTABLE.obtenerCuentaContableMaderas();
    res.json(cuentascontables)
  } catch (err) {
    console.log(err);
    res.status(500).send({    
      message: err || errorDb,
    });
  }
};


/**
 * Obtiene las ordenes de compra y existencia de la lista de los articulos
 * @param {*} req 
 * @param {*} res 
 */
exports.proyeccionInsumos = async (req, res) => {
  const { insumos, fechaI,fechaF } = req.body;
  try { 
    let ordenes = await models.PROYECCION.obtenerOrdenCompraInsumos(fechaI,fechaF,insumos);   
    let existencias = await models.PROYECCION.existenciaInsumos(insumos);

    existencias.map(ins => {
      const insumo = ordenes.filter( ord => ord.codigo === ins.codigo);
      if(insumo.length > 0){
        ins.ordenado = insumo[0].ordenado;
      }else {
        ins.ordenado = 0;
      }
    })

    res.json({ insumos:existencias })
  } catch (err) {
    console.log(err);
    res.status(500).send({    
      message: err.message || errorDb,
    });
  }
};

/**
 * Crear la Requisicion de cedulas en maderas
 * @param {*} req 
 * @param {*} res 
 */
 exports.crearRequisicion = async (req, res) => {
  const { REFERENCIA, INSUMOS } = req.body;
  try {    
    // Fecha
    const FECHA = getFecha();
    // Obtener el consecutivo
    let consecutivo = await models.DOCUMENTO_INV.últimoID();
    // Formatea el consecutivo para obtener el siguiente
    consecutivo = formatearConsecutivo(consecutivo[0].DOCUMENTO_INV);
    // Objeto del papa de requisicion
    let obtDocumentoInv = {
      REFERENCIA,
      SELECCIONADO: "N",
      MENSAJE_SISTEMA: "",
      CONSECUTIVO: "CONSUMO",    
      DOCUMENTO_INV: consecutivo,
      FECHA_HOR_CREACION: `${FECHA} ${getHora()}`,
      FECHA_DOCUMENTO: `${FECHA} ${getHora()}`,
      RecordDate	:	`${FECHA} ${getHora()}`,
      CreateDate	:	`${FECHA} ${getHora()}`,

      // ******************** CAMBIAR ESTO
      USUARIO: "ASISTLECHE",
      PAQUETE_INVENTARIO: "SFM",
      CreatedBy	:	'CI/ASISTLECHE',
      UpdatedBy	:	'CI/ASISTLECHE',
     
    }
     // Creacion del paquete
     await models.DOCUMENTO_INV.create(obtDocumentoInv);
    let contador = 1;
    // Recorre todas la lineas de insumos de la racion
    for(const insumo of INSUMOS){
      let obtLineaDocInv = {
      DOCUMENTO_INV: consecutivo,     
      LINEA_DOC_INV: contador,
      ARTICULO: insumo.codigo,      
      TIPO: "C",
      SUBTIPO: "D",
      SUBSUBTIPO: "N",
      CANTIDAD: insumo.cantidadaplicada,
      LOCALIZACION: "001",
      COSTO_TOTAL_LOCAL: 0,
      COSTO_TOTAL_DOLAR: 0,
      PRECIO_TOTAL_LOCAL: 0,
      PRECIO_TOTAL_DOLAR: 0,
      COSTO_TOTAL_LOCAL_COMP: 0,
      COSTO_TOTAL_DOLAR_COMP: 0,
      CENTRO_COSTO: insumo.centro_costo,

      // ******************** CAMBIAR ESTO
      BODEGA: insumo.bodega,
      PAQUETE_INVENTARIO: "SFM",
      CUENTA_CONTABLE: insumo.cuenta_contable,
      CreatedBy	:	'CI/ASISTLECHE'	,
      UpdatedBy	:	'CI/ASISTLECHE'	,

      AJUSTE_CONFIG: "~CC~",
      NoteExistsFlag: 0,
      RecordDate	:	`${FECHA} ${getHora()}`,     
      CreateDate	:	`${FECHA} ${getHora()}`
      }
       // Creacion de las línea
       await models.LINEA_DOC_INV.create(obtLineaDocInv)
      contador +=1;
    }
    // Actualiza el consecutivo de Consumo
    await  models.CONSECUTIVO_FA.updateConsumo(consecutivo);

    res.json(consecutivo);
    
  } catch (err) {
    console.log(err);
    res.status(500).send({    
      message: err || errorDb,
    });
  }
};

exports.obtenerProyectosFases = async (req, res) => {
  try {    
    const proyectos = await models.ARTICULO.obtenerProyectosMadera();  
    const fases = await models.ARTICULO.obtenerFasesMadera();   
    res.json({proyectos, fases})
  } catch (err) {
    console.log(err);
    res.status(500).send({    
      message: err || errorDb,
    });
  }
};

exports.obtenerFasesLabores = async (req, res) => {
  try {     
    const fases = await models.ARTICULO.obtenerFasesMaderaArticulos()
    res.json({fases})
  } catch (err) {
    console.log(err);
    res.status(500).send({    
      message: err || errorDb,
    });
  }
};
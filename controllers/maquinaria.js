const models = require("../models");

// Maquinarias
exports.obtenerMaquinarias = async (req, res) => {
  try {    
    const maquinaria = await models.ACTIVO_FIJO.obtenerMaquinaria();   
    res.json(maquinaria)
  } catch (err) {
    console.log(err);
    res.status(500).send({    
      message: err || errorDb,
    });
  }
};
